CashSale = {
    context: nlapiGetContext(),
    cashSale: null,
    giftCertificateNumber: null,
    setHeader: false,
    afterSubmit: function (type) {
        'use strict';
        if (type.toString() === 'create') {
            if (this.isBlank(nlapiGetFieldValue('createdfrom')) && parseInt(this.context.getRole(), 10) === parseInt(this.context.getSetting('SCRIPT', 'custscript_roleid'), 10)) {
                //I must load the record in the AfterSubmit, in order to copy the newly assigned Gift Card values
                this.cashSale = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
                for (var i = 1, lineCount = parseInt(this.cashSale.getLineItemCount('item'), 10); i <= lineCount; i = i + 1) {
                    this.giftCertificateNumber = this.cashSale.getLineItemValue('item', 'giftcertnumber', i);
                    if (this.isNotBlank(this.giftCertificateNumber)) {
                        if (!this.setHeader) {
                            this.setHeader = true;
                            this.cashSale.setFieldValue('custbody_first_gift_cert_code',this.giftCertificateNumber);
                        }
                        this.cashSale.setLineItemValue('item', 'custcol_gift_cert_code_col', i, this.giftCertificateNumber );
                    }
                }
                nlapiSubmitRecord(this.cashSale);
                nlapiSetRedirectURL('RECORD', nlapiGetRecordType(), nlapiGetRecordId(), false);
            }
        }
    },
    isBlank: function (s) {
        return s === undefined || s === null || s === '' || s.length < 1;
    },
    isNotBlank: function (s) {
        return !this.isBlank(s);
    },
    isNumber: function (n) {
        return this.isNotBlank(n) && !isNaN(parseInt(n, 10)) && isFinite(n);
    }
};
