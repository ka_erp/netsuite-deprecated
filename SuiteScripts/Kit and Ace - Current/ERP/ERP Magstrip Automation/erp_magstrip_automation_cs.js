/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Dec 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Boolean} True to continue changing field value, false to abort value change
 */
function erp_magstrip_automation_ValidateField(type, name, linenum){
	
	giftCard_validateField(type,name);
    
}


function giftCard_validateField(type,name){
	
	var exec_context = nlapiGetContext().getExecutionContext();
	nlapiLogExecution("AUDIT", "CONTEXT", JSON.stringify(exec_context));
			
	if(exec_context == "userinterface" ){	
	
					
	if(name == 'custbody_erp_magstrip'){
		

			var magStrip = nlapiGetFieldValue('custbody_erp_magstrip');

			if (magStrip != "") {

				var objSwipedObject = SwipeParserObj(magStrip);

				if (objSwipedObject) {

					var authcode = objSwipedObject.goldnugget;

					if (authcode) {

						var targetLine = 0;
						var bFound = false;
						var itemCount = nlapiGetLineItemCount('item');

						for (var order = 1; order <= itemCount; order++) {
							var sublistItem = nlapiGetLineItemValue('item',
									'itemtype', order);

							if (sublistItem == 'GiftCert') {
								var giftCode = nlapiGetLineItemValue('item',
										'giftcertnumber', order);

								if ((giftCode == null || giftCode == "")
										&& (bFound == false)) {
									targetLine = order;
									bFound = true;
								}
							}
						}

						if (targetLine > 0) {

							nlapiSelectLineItem('item', targetLine);
							nlapiSetCurrentLineItemValue('item','giftcertnumber', authcode);

							setTimeout(function() {
								nlapiCommitLineItem('item');
							}, 1000);

							nlapiSetFieldValue('custbody_erp_magstrip', "");
						}

					} else {
						alert("error on magstrip entry. authcode was not found");						
					}
				}

				return true;

			} else {

				return true;
			}
			
		}else{
			
			return true;
		}
		
		}
		else{
			
			return true;
		}
	}



function SwipeParserObj(strParse)
{

	var objParsed = new Object(); 
	
	//console.log('Testing '); 
    ///////////////////////////////////////////////////////////////
    ///////////////////// member variables ////////////////////////
    objParsed.input_trackdata_str = strParse;
    objParsed.account_name = null;
    objParsed.surname = null;
    objParsed.firstname = null;
    objParsed.acccount = null;
    objParsed.exp_month = null;
    objParsed.exp_year = null;
    objParsed.track1 = null;
    objParsed.track2 = null;
    objParsed.hasTrack1 = false;
    objParsed.hasTrack2 = false;
    /////////////////////////// end member fields /////////////////
    
    
    sTrackData = objParsed.input_trackdata_str;     //--- Get the track data
    
  //-- Example: Track 1 & 2 Data
  //-- %B1234123412341234^CardUser/John^030510100000019301000000877000000?;1234123412341234=0305101193010877?
  //-- Key off of the presence of "^" and "="

  //-- Example: Track 1 Data Only
  //-- B1234123412341234^CardUser/John^030510100000019301000000877000000?
  //-- Key off of the presence of "^" but not "="

  //-- Example: Track 2 Data Only
  //-- 1234123412341234=0305101193010877?
  //-- Key off of the presence of "=" but not "^"

   // console.log('YEY!'); 
    
  if ( strParse != '' )
  {
    // alert(strParse);

    //--- Determine the presence of special characters
    nHasTrack1 = strParse.indexOf("^");
    nHasTrack2 = strParse.indexOf("=");
    
    //console.log(nHasTrack1);
    //console.log(nHasTrack2);

    //--- Set boolean values based off of character presence
    objParsed.hasTrack1 = bHasTrack1 = false;
    objParsed.hasTrack2 = bHasTrack2 = false;
    if (nHasTrack1 > 0) { objParsed.hasTrack1 = bHasTrack1 = true; }
    if (nHasTrack2 > 0) { objParsed.hasTrack2 = bHasTrack2 = true; }

    //--- Test messages
    // alert('nHasTrack1: ' + nHasTrack1 + ' nHasTrack2: ' + nHasTrack2);
    // alert('bHasTrack1: ' + bHasTrack1 + ' bHasTrack2: ' + bHasTrack2);    

    //--- Initialize
    bTrack1_2  = false;
    bTrack1    = false;
    bTrack2    = false;

    
  //  console.log(1); 
    //--- Determine tracks present
    if (( bHasTrack1) && ( bHasTrack2)) { bTrack1_2 = true; }
    if (( bHasTrack1) && (!bHasTrack2)) { bTrack1   = true; }
    if ((!bHasTrack1) && ( bHasTrack2)) { bTrack2   = true; }

  //  console.log(2); 
    //--- Test messages
    // alert('bTrack1_2: ' + bTrack1_2 + ' bTrack1: ' + bTrack1 + ' bTrack2: ' + bTrack2);

    //--- Initialize alert message on error
    bShowAlert = false;
    
    //-----------------------------------------------------------------------------    
    //--- Track 1 & 2 cards
    //--- Ex: B1234123412341234^CardUser/John^030510100000019301000000877000000?;1234123412341234=0305101193010877?
    //-----------------------------------------------------------------------------    
    if (bTrack1_2)
    { 
//      alert('Track 1 & 2 swipe');

      strCutUpSwipe = '' + strParse + ' ';
      arrayStrSwipe = new Array(4);
      arrayStrSwipe = strCutUpSwipe.split("^");
  
      var sAccountNumber, sName, sShipToName, sMonth, sYear;
  
      
      
      if ( arrayStrSwipe.length > 2 )
      {
        objParsed.account = stripAlpha( arrayStrSwipe[0].substring(1,arrayStrSwipe[0].length) );
        objParsed.account_name          = arrayStrSwipe[1];
        objParsed.exp_month         = arrayStrSwipe[2].substring(2,4);
        objParsed.exp_year          = '20' + arrayStrSwipe[2].substring(0,2);
        
        objParsed.array = arrayStrSwipe;         
        
        objParsed.ignorethis =  arrayStrSwipe[2].substring(0,5);
        objParsed.goldnugget =  arrayStrSwipe[2].substring(5,14);
        
        //console.log(JSON.stringify(objParsed));
        
       
        
        //--- Different card swipe readers include or exclude the % in the front of the track data - when it's there, there are
        //---   problems with parsing on the part of credit cards processor - so strip it off
        if ( sTrackData.substring(0,1) == '%' ) {
            sTrackData = sTrackData.substring(1,sTrackData.length);
        }

           var track2sentinel = sTrackData.indexOf(";");
           if( track2sentinel != -1 ){
               objParsed.track1 = sTrackData.substring(0, track2sentinel);
               objParsed.track2 = sTrackData.substring(track2sentinel);
           }

        //--- parse name field into first/last names
        var nameDelim = objParsed.account_name.indexOf("/");
        if( nameDelim != -1 ){
            objParsed.surname = objParsed.account_name.substring(0, nameDelim);
            objParsed.firstname = objParsed.account_name.substring(nameDelim+1);
        }
        
        //return objParsed;
      }
      else  //--- for "if ( arrayStrSwipe.length > 2 )"
      { 
        bShowAlert = true;  //--- Error -- show alert message
      }
    }
    
    //-----------------------------------------------------------------------------
    //--- Track 1 only cards
    //--- Ex: B1234123412341234^CardUser/John^030510100000019301000000877000000?
    //-----------------------------------------------------------------------------    
    if (bTrack1)
    {
//      alert('Track 1 swipe');

      strCutUpSwipe = '' + strParse + ' ';
      arrayStrSwipe = new Array(4);
      arrayStrSwipe = strCutUpSwipe.split("^");
  
      var sAccountNumber, sName, sShipToName, sMonth, sYear;
  
      if ( arrayStrSwipe.length > 2 )
      {
        objParsed.account = sAccountNumber = stripAlpha( arrayStrSwipe[0].substring( 1,arrayStrSwipe[0].length) );
        objParsed.account_name = sName    = arrayStrSwipe[1];
        objParsed.exp_month = sMonth    = arrayStrSwipe[2].substring(2,4);
        objParsed.exp_year = sYear    = '20' + arrayStrSwipe[2].substring(0,2); 
        objParsed.ignorethis =  arrayStrSwipe[2].substring(0,5);
        objParsed.goldnugget =  arrayStrSwipe[2].substring(6,11);
        
        //--- Different card swipe readers include or exclude the % in
        //--- the front of the track data - when it's there, there are
        //---   problems with parsing on the part of credit cards processor - so strip it off
        if ( sTrackData.substring(0,1) == '%' ) { 
            objParsed.track1 = sTrackData = sTrackData.substring(1,sTrackData.length);
        }
  
        //--- Add track 2 data to the string for processing reasons
//        if (sTrackData.substring(sTrackData.length-1,1) != '?')  //--- Add a ? if not present
//        { sTrackData = sTrackData + '?'; }
        objParsed.track2 = ';' + sAccountNumber + '=' + sYear.substring(2,4) + sMonth + '111111111111?';
        sTrackData = sTrackData + objParsed.track2;
  
        //--- parse name field into first/last names
        var nameDelim = objParsed.account_name.indexOf("/");
        if( nameDelim != -1 ){
            objParsed.surname = objParsed.account_name.substring(0, nameDelim);
            objParsed.firstname = objParsed.account_name.substring(nameDelim+1);
        }
        
        return objParsed;

      }
      else  //--- for "if ( arrayStrSwipe.length > 2 )"
      { 
        bShowAlert = true;  //--- Error -- show alert message
      }
    }
    
    //-----------------------------------------------------------------------------
    //--- Track 2 only cards
    //--- Ex: 1234123412341234=0305101193010877?
    //-----------------------------------------------------------------------------    
    if (bTrack2)
    {
//      alert('Track 2 swipe');
    
      nSeperator  = strParse.indexOf("=");
      sCardNumber = strParse.substring(1,nSeperator);
      sYear       = strParse.substr(nSeperator+1,2);
      sMonth      = strParse.substr(nSeperator+3,2);

      // alert(sCardNumber + ' -- ' + sMonth + '/' + sYear);

      objParsed.account = sAccountNumber = stripAlpha(sCardNumber);
      objParsed.exp_month = sMonth        = sMonth;
      objParsed.exp_year = sYear            = '20' + sYear; 
        
      //--- Different card swipe readers include or exclude the % in the front of the track data - when it's there, 
      //---  there are problems with parsing on the part of credit cards processor - so strip it off
      if ( sTrackData.substring(0,1) == '%' ) {
        sTrackData = sTrackData.substring(1,sTrackData.length);
      }
  
    }
    
    //-----------------------------------------------------------------------------
    //--- No Track Match
    //-----------------------------------------------------------------------------    
    if (((!bTrack1_2) && (!bTrack1) && (!bTrack2)) || (bShowAlert))
    {
      //alert('Difficulty Reading Card Information.\n\nPlease Swipe Card Again.');
    }

//    alert('Track Data: ' + document.formFinal.trackdata.value);
    
    //document.formFinal.trackdata.value = replaceChars(document.formFinal.trackdata.value,';','');
    //document.formFinal.trackdata.value = replaceChars(document.formFinal.trackdata.value,'?','');

//    alert('Track Data: ' + document.formFinal.trackdata.value);

  } //--- end "if ( strParse != '' )"


  	return objParsed; 
  	
    objParsed.dump = function(){
        var s = "";
        var sep = "\r"; // line separator
        s += "Name: " + objParsed.account_name + sep;
        s += "Surname: " + objParsed.surname + sep;
        s += "first name: " + objParsed.firstname + sep;
        s += "account: " + objParsed.account + sep;
        s += "exp_month: " + objParsed.exp_month + sep;
        s += "exp_year: " + objParsed.exp_year + sep;
        s += "has track1: " + objParsed.hasTrack1 + sep;
        s += "has track2: " + objParsed.hasTrack2 + sep;
        s += "TRACK 1: " + objParsed.track1 + sep;
        s += "TRACK 2: " + objParsed.track2 + sep;
        s += "Raw Input Str: " + objParsed.input_trackdata_str + sep;
        
        return s;
    }

    
    
    //return objParsed;

}

function stripAlpha(sInput){
    if( sInput == null )    return '';
    return sInput.replace(/[^0-9]/g, '');
}