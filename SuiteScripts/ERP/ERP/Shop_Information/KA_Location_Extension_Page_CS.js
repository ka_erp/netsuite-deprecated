/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Aug 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum){
 
}


function fieldChanged_LocationExtension(type,name){
	
	//alert('changed ' + name);
	if(name == 'custpage_day_all'){
		
		var isAll = nlapiGetFieldValue('custpage_day_all'); 
				
		 nlapiSetFieldValue('custpage_day_monday',isAll);		
		 nlapiSetFieldValue('custpage_day_tuesday',isAll);
		 nlapiSetFieldValue('custpage_day_wednesday',isAll);
		 nlapiSetFieldValue('custpage_day_thursday',isAll);
		 nlapiSetFieldValue('custpage_day_friday',isAll);
		 nlapiSetFieldValue('custpage_day_saturday',isAll);
		 nlapiSetFieldValue('custpage_day_sunday',isAll);		
	}
	
	
	if(name == 'custpage_location_name'){ //invoice

		
		var strRedirectURL = nlapiResolveURL( 'SUITELET', 'customscript_erp_location_extension', 'customdeploy_erp_location_extension' );
		
		var currURL = window.location; 
		
		strRedirectURL = strRedirectURL + '&param_location='+ nlapiGetFieldValue('custpage_location_name');
		
		
		console.log(strRedirectURL);
		//window.open(strRedirectURL + '&param_location='+ nlapiGetFieldValue('custpage_location_name'));
		
		
		//var a = document.forms['main_form'].submitted.value; 
		
//		console.log(document.forms['main_form'].submitted.value);
//		
//		document.forms['main_form'].submitted.value = 'T';
//		
//		console.log(document.forms['main_form'].submitted.value);
		
		
		
		window.location.replace(strRedirectURL);
		
//		document.forms['main_form'].submit();
		
//		var intRecordID = nlapiGetFieldValue('custpage_location_name');
//		console.log("1");
//
//		var objLocationRecord = nlapiLoadRecord('location',intRecordID); 
//
//		if(!isNullOrEmpty(objLocationRecord)){
//		
//	     console.log("2");
//			
//		 var strLocId = objLocationRecord.getId();
//		 
//		 
////		 console.log(JSON.stringify(objLocationRecord));
//		 
//		 
//		 var strStorePhone = objLocationRecord.getFieldValue('custrecord_ka_store_phone'); 
//		 var strShopDirector = objLocationRecord.getFieldValue('custrecord_ka_shop_director'); 
//		 
////		 		 
//		 var emailShopEmailAddress = nlapiLookupField('location', strLocId, 'custrecord_ka_shop_email' ); 	
//		 
//		 
//		 
//		 var txtAreaShopAddress = objLocationRecord.getFieldValue('mainaddress_text');		 				 		
//		 var phnShopDirectorPhone = objLocationRecord.getFieldValue('custrecord_ka_shop_director_contact'); 
//		 		 
//         var strPaymentProcessor = nlapiLookupField('location', strLocId, 'custrecord_ka_payment_processor' ,true); 	
//         var strInternetProvider = nlapiLookupField('location', strLocId, 'custrecord_ka_internet_provider' ,true);         
//         var strPhoneProvider =    nlapiLookupField('location', strLocId, 'custrecord_ka_phone_provider' ,true);
//         var strSensormaticProvider = nlapiLookupField('location', strLocId, 'custrecord_ka_sensormatic_provider',true);
//         var strMusicProvider = nlapiLookupField('location', strLocId, 'custrecord_ka_music_provider',true);
//         
//         
//		 var strPaymentProcessorContact = objLocationRecord.getFieldValue('custrecord_ka_payment_processor_phone');				 
//		 var strPhoneProviderContact = objLocationRecord.getFieldValue('custrecord_ka_phone_contact');		 
//		 var strInternetProviderContact = objLocationRecord.getFieldValue('custrecord_ka_internet_contact');		 
//		 var strSensormaticProviderConact = objLocationRecord.getFieldValue('custrecord_sensormatic_contact');		 
//		 var strMusicProviderConact = objLocationRecord.getFieldValue('custrecord_ka_music_provider_contact');
//		 
//		 var strIPAddress = objLocationRecord.getFieldValue('custrecord_ka_ip_addresss'); 
//		 var strMerchandiseID = objLocationRecord.getFieldValue('custrecord_ka_merchant_id');
//		 
//		 		 		
////		 console.log("emailShopEmailAddress " + emailShopEmailAddress); 
//		
//		 nlapiSetFieldValue('custpage_email_address', emailShopEmailAddress );
//		 nlapiSetFieldValue('custpage_shop_address', txtAreaShopAddress);
//		 
//		 
//		 
//		 
//		 if(!isNullOrEmpty(strShopDirector)){
//			 nlapiSetFieldValue('custpage_shop_director', strShopDirector);
//		 }else{
//			 nlapiSetFieldValue('custpage_shop_director', "0");
//		 }
//		 
//		 nlapiSetFieldValue('custpage_shop_director_phone', phnShopDirectorPhone);
//		 nlapiSetFieldValue('custpage_store_phone', strStorePhone);
//		
//		 
//		 nlapiSetFieldValue('custpage_payment_provider', strPaymentProcessor);
//		 nlapiSetFieldValue('custpage_payment_provider_phone', strPaymentProcessorContact);
//		 nlapiSetFieldValue('custpage_payment_provider_merchid', strMerchandiseID);
//		 nlapiSetFieldValue('custpage_ip_address', strIPAddress);		 
//		 nlapiSetFieldValue('custpage_phone_provider', strPhoneProvider);
//		 nlapiSetFieldValue('custpage_phone_provider_phone', strPhoneProviderContact);
//		 nlapiSetFieldValue('custpage_internet_provider', strInternetProvider);
//		 nlapiSetFieldValue('custpage_internet_provider_phone', strInternetProviderContact);
//		 nlapiSetFieldValue('custpage_sensormatic_provider', strSensormaticProvider);
//		 nlapiSetFieldValue('custpage_sensormatic_provider_phone', strSensormaticProviderConact);
//		 nlapiSetFieldValue('custpage_music_provider', strMusicProvider);
//		 nlapiSetFieldValue('custpage_music_provider_phone', strMusicProviderConact);
 
 
		 	
		
//		}
		   
	}
}

function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}