/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Aug 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function generateTrackingUserEventBeforeSubmit(type){
 
	
	var intPackageCount = nlapiGetLineItemCount('package');
	var arrPackageDetails = new Array();
	
	for (var i = 1  ;  i <= intPackageCount; i++){
		nlapiSelectLineItem('package', i);
		var strTracking = nlapiGetCurrentLineItemValue('package', 'packagetrackingnumber');
		
		arrPackageDetails.push(strTracking);
	}
	
	
	nlapiSetFieldValue(arrPackageDetails); 
	
	
}
