/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Dec 2015     ivan.sioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

var listDetails = new Array(); 

function erp_PurchaseOrder_ListFields_SL(request, response){

	
	var listArray = ["customlist_ka_shipping_method","customrecord_wfx_season","customlist_ka_po_type","customlist_ka_shipping_port","customrecord_ka_channel_list"];  
	var listArrayNames = ["Shipping Method","Season","PO Type","Shipping Port","Channel"];
	
	var list = ''; 
		
	for (var i = 0 ; i < listArray.length; i++ ){
	
		var listName = listArray[i];

		
		if(!isNullOrEmpty(listName)){
	
		try{
			list = listArray[i]; listText = listArrayNames[i];
			enumerateListItems(list,listText, 'name');
		}
		catch(ex){
			nlapiLogExecution('ERROR', 'FAIL', ex.toString()); 
			
		}
		}
	}	
	
	createCSV(); 	
	
}


function enumerateListItems(listName, listText, listfield){
	
	var col = new Array(); //var listname = 'customlist_ka_channel_code'; var listtext = 'CAN'; 	
	col[0] = new nlobjSearchColumn(listfield);
	col[1] = new nlobjSearchColumn('internalId');
	
	
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F'); 		 
	
	var results = nlapiSearchRecord(listName, null, filter, col);
	
	
	if(!isNullOrEmpty(results)){
		
		for ( var i = 0; results != null && i < results.length; i++ )
		{
			var listResultObject = new Object();
			
			var res = results[i]; 
			listResultObject.listName = listName;
			listResultObject.listText = listText;
			listResultObject.listValue = (res.getValue(listfield));
			listResultObject.listID = (res.getValue('internalId'));
			
			 
			
			//addToListObject(listResultObject);
			
			listDetails.push(listResultObject); 
			
		} 	
		
	}
			
}

function addToListObject(result) {
		
	
}


function createCSV(){
	
	
	if(!isNullOrEmpty(listDetails)){
		
		var csvFileName = 'LIST_' + createDate().toString() + '.csv';
		var csvDetails =  'List Name, Name , List Id, Internal Id \n' ; 
			
		var folderId  = '';
		
		if (isNullOrEmpty(folderId)){
			folderId = '-15'; //Default to SuiteScripts folder if blank
		}
		
		
		//Construct
		
		for (var i = 0; i < listDetails.length; i++) {
			var obj = listDetails[i]; 
			
			csvDetails += obj.listText + ',';
			csvDetails += obj.listValue + ',';
			csvDetails += obj.listName + ',';
			csvDetails += obj.listID+ '\n';
			
			
		}
		
			
		var csvFile = nlapiCreateFile(csvFileName, 'CSV', csvDetails);
		csvFile.setFolder(folderId);
		var csvId = nlapiSubmitFile(csvFile);
		nlapiLogExecution('DEBUG', 'CSV File', 'File Id: ' + csvId);
		
	}
	
		
}


function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}


function createDate() {
	var date = new Date();
	
	var newDate = nlapiDateToString(new Date(), "datetimetz");

//	var datetime = date.getFullYear().toString();
//	datetime += (parseInt(date.getMonth()) + 1).toString();
//	datetime += String('00' + date.getDate().toString()).slice(-2);

	return newDate;
}