/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Dec 2015     ivan.sioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function erpPurchaseOrderStaging_SL(request, response){

	

	nlapiLogExecution('DEBUG', 'erpPurchaseOrderStaging_SL', 'enter this'  +  new Date().toLocaleString()); 
	var transID = request.getParameter( 'transactionRecord' );
	nlapiLogExecution('DEBUG', 'erpPurchaseOrderStaging_SL transID', transID);
	
	try{
		
		if(transID){
	 		  
			var purchaseOrder = nlapiLoadRecord('purchaseorder', transID );	
			purchaseOrder.setFieldValue('custbody_ka_process_execution', 'T'); 
			nlapiSubmitRecord(purchaseOrder); 
                        
			nlapiLogExecution('DEBUG', 'erpPurchaseOrderStaging_SL', "after calling " +  new Date().toLocaleString()); 
			//nlapiSetRedirectURL('RECORD', 'purchaseorder', transID, false);					    
		    
		}		
	}catch(ex){		
		nlapiLogExecution('DEBUG', 'intercoPOCreation_RunPO_UE_SL: Error', ex);
	}	
	
	
}


function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}
