/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Sep 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function intercompany_SalesOrder_BS(type){
 
	try{
		var poID = nlapiGetFieldValue('intercotransaction');		
		nlapiLogExecution('DEBUG', 'poID', poID); 
		
		if(poID){
		
			var memoPO = nlapiLookupField('purchaseorder', poID, 'memo');
			nlapiLogExecution('DEBUG', 'memoPO', memoPO); 
			nlapiSetFieldValue('memo', memoPO); 
		
		}
	}
	catch(ex){
		nlapiLogExecution('DEBUG', 'Exception Occurred', ex);
		
	}
}

function intercompany_SalesOrder_AS(type){

	
	var soID = nlapiGetRecordId(); 
	var salesOrder_POLink = nlapiLookupField('salesorder', soID, 'intercotransaction');
	
	var memoPO = nlapiLookupField('purchaseorder', salesOrder_POLink, 'memo');
	nlapiSubmitField('salesorder', soID, 'memo', memoPO); 
}

