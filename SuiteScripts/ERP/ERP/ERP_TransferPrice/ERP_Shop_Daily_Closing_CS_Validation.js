/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.02       11 Dec 2015     ivan.sioson
 * 1.03       May 5 2016 Tim Frazer - added AUR field and fixed reloading of goals on edit
 */


function erp_sdc_PageSave(){



	var closingNotesForm = nlapiGetFieldValue('customform'); //var conf = false;
	nlapiLogExecution('AUDIT','FORM',closingNotesForm);

	if(closingNotesForm == 132){


		if(nlapiGetFieldValue('custrecord_erp_complete') == 'T'){

			  var conf = true;

				/*
				var varianceNSTerminal = nlapiGetFieldValue('custrecord_erp_dclose_variance');
				if(isNullOrEmpty(varianceNSTerminalReason)) varianceNSTerminal = 0;

				var varianceNSTerminalReason = nlapiGetFieldValue('custrecord_erp_dclose_variance_reason');
				if (varianceNSTerminal != 0 && isNullOrEmpty(varianceNSTerminalReason)){
					 conf = confirm('Are you sure you want to submit with variance reason not set');
				}
				*/

				var dailySales = nlapiGetFieldValue('custrecord_erp_dclose_weekly_actual');
				if(isNullOrEmpty(dailySales) || dailySales ==  0){
					conf = confirm('Please verify your sales are correctly reporting as $0');
				}

		    return conf;

		}else{return true;}


	} else{

		return true;
	}

}
