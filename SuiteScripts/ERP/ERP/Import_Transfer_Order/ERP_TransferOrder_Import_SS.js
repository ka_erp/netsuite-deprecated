/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Sep 2015     ivan.sioson
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function executeTransferOrderStaging_scheduled(type) {

	nlapiSubmitField('customrecord_tran_order_staging_header', 4412 , 'custrecord_erp_create_transfer_order', 'T'); 
	
}
