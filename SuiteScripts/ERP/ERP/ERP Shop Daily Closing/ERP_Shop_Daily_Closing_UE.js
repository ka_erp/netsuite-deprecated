/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       11 Jan 2016     ivan.sioson
 * 2.00       9 Jan 2017      tim.frazer - added international support
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */

function erp_sdc_UEBS(){





	var closingNotesForm = nlapiGetFieldValue('customform');
	if(closingNotesForm == 124){


	var idLocation = nlapiGetLocation();
	nlapiSetFieldValue('custrecord_erp_dclose_shop_name', idLocation);

	}

var strShopName = nlapiGetFieldText('custrecord_erp_dclose_shop_name');



								if(!isNullOrEmpty(strShopName)){
									var arrShopName = strShopName.split(":");
									var lastContent = arrShopName.length; lastContent =  lastContent - 1;
									var shortName = arrShopName[lastContent];

									nlapiSetFieldValue('custrecord_erp_dclose_shop_name_short', shortName);
								}


}

function erp_sdc_UEAS(){


	var closingNotesForm = nlapiGetFieldValue('customform');
	if(closingNotesForm == 124){


			var soId
			,	emailMerger
			,	mergeResult
			,	templateId
			,	emailBody
			,	emailSender
			,	emailSubject
			,	emailRecipient;

			soId = nlapiGetRecordId();


			nlapiLogExecution('AUDIT', 'erp_sdc_UEAS: Sending Email ', soId);

			var bComplete = nlapiLookupField('customrecord_erp_shop_daily_closing', soId, 'custrecord_erp_complete'  ); //nlapiGetFieldValue('custrecord_erp_complete');
			var strStoreShortname = nlapiLookupField('customrecord_erp_shop_daily_closing', soId, 'custrecord_erp_dclose_shop_name_short'  ); //nlapiGetFieldValue('custrecord_erp_complete');
			var strDateCreated = nlapiLookupField('customrecord_erp_shop_daily_closing', soId, 'custrecord_erp_date_created'  ); //nlapiGetFieldValue('custrecord_erp_complete');
			strDateCreated  = moment(strDateCreated).format('LL');


			if(bComplete == 'T'){

				try {

					emailSender = nlapiGetUser(); //1549;
					emailSubject = 'Daily Closing Notes : ' + strStoreShortname  + ' - ' + strDateCreated;


					locaitonId = nlapiLookupField('customrecord_erp_shop_daily_closing', soId, 'custrecord_erp_dclose_shop_name'  ); //nlapiGetFieldValue('custrecord_erp_dclose_emailrecipient');

					if(!isNullOrEmpty(locaitonId)){

						emailRecipient = nlapiLookupField('location', locaitonId, 'custrecord_erp_dclose_loc_emailuser');
						nlapiLogExecution('AUDIT', 'erp_sdc_UEAS: emailRecipient ', emailRecipient);

						if(!isNullOrEmpty(emailRecipient)){


							/*if(locaitonId == 70){
								emailMerger = nlapiCreateEmailMerger(26);
							}else{
								emailMerger = nlapiCreateEmailMerger(20);
							}*/

				emailMerger = nlapiCreateEmailMerger(26);

							emailMerger.setCustomRecord('customrecord_erp_shop_daily_closing', soId);

							var records = new Object();
							records['recordtype'] = 'customrecord_erp_shop_daily_closing';  // for example 55
							records['record'] = soId ;

							mergeResult = emailMerger.merge();
							emailBody = mergeResult.getBody();

							var sendEmail = nlapiSendEmail(emailSender, emailRecipient, emailSubject, emailBody, null, null, records);

							nlapiLogExecution('AUDIT', 'erp_sdc_UEAS: Email ', sendEmail);

						}

					}


				} catch(e) {

					nlapiLogExecution('ERROR', 'Error sending Sales Order notification email', e);
				}


			}
	}

	if(closingNotesForm != 124){


		var soId
		,	emailMerger
		,	mergeResult
		,	templateId
		,	emailBody
		,	emailSender
		,	emailSubject
		,	emailRecipient;

		soId = nlapiGetRecordId();


		nlapiLogExecution('AUDIT', 'erp_sdc_UEAS: Sending Email ', soId);

		var bComplete = nlapiLookupField('customrecord_erp_shop_daily_closing', soId, 'custrecord_erp_complete'  ); //nlapiGetFieldValue('custrecord_erp_complete');
		var strStoreShortname = nlapiLookupField('customrecord_erp_shop_daily_closing', soId, 'custrecord_erp_dclose_shop_name_short'  ); //nlapiGetFieldValue('custrecord_erp_complete');
		var strDateCreated = nlapiLookupField('customrecord_erp_shop_daily_closing', soId, 'custrecord_erp_date_created'  ); //nlapiGetFieldValue('custrecord_erp_complete');
		strDateCreated  = moment(strDateCreated).format('LL');


		var todayActual = nlapiLookupField('customrecord_erp_shop_daily_closing', soId, 'custrecord_erp_dclose_weekly_goal'  );
		todayActual = parseInt(todayActual);


		if(bComplete == 'T'){

			try {




				emailSender = nlapiGetUser(); //1549;
				emailSubject = 'Daily Closing Notes : ' + strStoreShortname  + ' - ' + strDateCreated;


				locaitonId = nlapiLookupField('customrecord_erp_shop_daily_closing', soId, 'custrecord_erp_dclose_shop_name'  ); //nlapiGetFieldValue('custrecord_erp_dclose_emailrecipient');

				if(!isNullOrEmpty(locaitonId)){

					emailRecipient = nlapiLookupField('location', locaitonId, 'custrecord_erp_dclose_loc_emailuser');
					nlapiLogExecution('AUDIT', 'erp_sdc_UEAS: emailRecipient ', emailRecipient);

					if(todayActual == 0 || (isNaN(todayActual) == true)){
						emailRecipient = nlapiGetUser();
						emailSubject = 'Daily Closing Notes : ' + strStoreShortname  + ' - ' + strDateCreated + ' : Error Occurred - Contact ERP/Shop Support';
					}


					nlapiLogExecution('AUDIT', 'erp_sdc_UEAS: todayActual ', todayActual);
					nlapiLogExecution('AUDIT', 'erp_sdc_UEAS: emailSubject ', emailSubject);

					if(!isNullOrEmpty(emailRecipient)){


						/*if(locaitonId == 70){
							emailMerger = nlapiCreateEmailMerger(26);
						}else{
							emailMerger = nlapiCreateEmailMerger(20);
						}*/

						emailMerger = nlapiCreateEmailMerger(30);

						emailMerger.setCustomRecord('customrecord_erp_shop_daily_closing', soId);

						var records = new Object();
						records['recordtype'] = 'customrecord_erp_shop_daily_closing';  // for example 55
						records['record'] = soId ;

						mergeResult = emailMerger.merge();
						emailBody = mergeResult.getBody();

						var sendEmail = nlapiSendEmail(emailSender, emailRecipient, emailSubject, emailBody, null, null, records);

						nlapiLogExecution('AUDIT', 'erp_sdc_UEAS: Email ', sendEmail);

					}

				}


			} catch(e) {

				nlapiLogExecution('ERROR', 'Error sending Sales Order notification email', e);
			}


		}
}
}

function erp_sdc_UEBL(type, form, request){

		var closingNotesForm = nlapiGetFieldValue('customform');
		nlapiLogExecution('AUDIT', 'erp_sdc_UEBL', 'I am the form: ' + closingNotesForm);

		if(closingNotesForm == 124){



						nlapiLogExecution('AUDIT', 'erp_sdc_UEBL', 'erp_sdc_UEBL 1');

						var idLocation = nlapiGetLocation();
						var objDates   = getAllDates();	setAllDates(objDates);

						nlapiLogExecution('AUDIT', 'erp_sdc_UEBL', 'erp_sdc_UEBL 2: idLocation ' + idLocation);

						if(type == 'create'){


							var bClosingUnique = uniqueClosingNotes(idLocation); 											//If not unique for the day for the location
							nlapiLogExecution('AUDIT', 'erp_sdc_UEBL', bClosingUnique);

							if(!bClosingUnique){
								 throw nlapiCreateError('CLOSING NOTE NOT UNIQUE', 'The closing note has been created, locate and edit the closing note for today');
							}
						}


						if(type == 'edit' || type == 'create' ){



						nlapiLogExecution('AUDIT', 'erp_sdc_UEBL', 'erp_sdc_UEBL 3: type ' + type);



						try{


								var recLocation = nlapiLoadRecord('location', idLocation);
								nlapiSetFieldValue('custrecord_erp_dclose_shop_name', idLocation);
								var strShopName = nlapiGetFieldText('custrecord_erp_dclose_shop_name');



								if(!isNullOrEmpty(strShopName)){
									var arrShopName = strShopName.split(":");
									var lastContent = arrShopName.length; lastContent =  lastContent - 1;
									var shortName = arrShopName[lastContent];

									nlapiSetFieldValue('custrecord_erp_dclose_shop_name_short', shortName);
								}


							 	if(!isNullOrEmpty(recLocation)){

									//CLOSING HEADER
									//nlapiLogExecution('DEBUG', 'setMainShopClosing', 'setMainShopClosing' + JSON.stringify(recLocation));
							 		setMainShopClosing(recLocation);


							 		//SALES
							 		nlapiLogExecution('DEBUG', 'setSalesInfo', 'setSalesInfo');
							 		setSalesInfo(idLocation, "daily", objDates.today, objDates.today);
							 		setSalesInfo(idLocation, "weekly", objDates.weekStart, objDates.weekEnd);
									setSalesInfo(idLocation, "period", objDates.periodStart , objDates.periodEnd);
									setSalesInfo(idLocation, "qtr",  objDates.qtrStart, objDates.qtrEnd);

									//Set the main fields (top most)

									//UPTSPT
							 		setPeakTimes(idLocation, objDates.today, objDates.today);
							 		var uptspt = getUPTSPT(idLocation, objDates.today, objDates.today);
							 		var salesClosing = getShopClosing(idLocation, objDates.today, objDates.today);

								}

							}catch(ex){

								nlapiLogExecution('ERROR', 'erp_sdc_UEBL', 'erp_sdc_UEBL ERROR: type ' + ex.toString());

							}

						}


		} else{

			if(type == 'create'){

							var currentDate = new Date();
							var todayDate = moment(currentDate).format('L');
							var idLocation = nlapiGetLocation();

							var bClosingUnique = uniqueClosingNotes(idLocation); 											//If not unique for the day for the location
							nlapiLogExecution('AUDIT', 'erp_sdc_UEBL', bClosingUnique);

							if(!bClosingUnique){
								 throw nlapiCreateError('CLOSING NOTE NOT UNIQUE', 'The closing note has been created, locate and edit the closing note for today');
							}
			}
		}

}



function uniqueClosingNotes(idLoc){

		 var currentDate  =  new Date();
		 currentDate    = moment(currentDate).format('L');
		 var userInfo = nlapiLoadConfiguration('userpreferences');
		 var userTimeZone = userInfo.getFieldValue('TIMEZONE');
		 nlapiLogExecution('DEBUG', 'userTimeZone Closing: ',currentDate);

		if(userTimeZone =='Australia/Sydney'){
			 currentDate = moment().add(18,'hours').format('L');
			nlapiLogExecution('DEBUG', 'userTime Closing Sydney: ',currentDate);

		}
		nlapiLogExecution('DEBUG', 'userTime Closing: ',currentDate+'userTimeZone '+userTimeZone);

try{

			var strSavedSearchIDItm = null;
	  	var arSaveSearchFiltersItm = new Array();
	  	var arSavedSearchColumnsItm = new Array();
	  	var arSavedSearchResultsItm = null;

	  	var arrResultObjectArray = new Array();


	  	arSaveSearchFiltersItm.push(new nlobjSearchFilter('custrecord_erp_dclose_shop_name', null , 'is', idLoc ));
			arSaveSearchFiltersItm.push(new nlobjSearchFilter('custrecord_erp_date_created', null , 'on' , currentDate)); //'within', [currentDate,currentDate] ));

	    arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid'));
	    arSavedSearchColumnsItm.push(new nlobjSearchColumn('externalid'));
	    //arSavedSearchColumnsItm.push(new nlobjSearchColumn('tranid'));


	  	arSavedSearchResultsItm = nlapiSearchRecord('customrecord_erp_shop_daily_closing', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);
			nlapiLogExecution('DEBUG', 'nlapiSearchRecord: ',arSavedSearchResultsItm);


	  	if(isNullOrEmpty(arSavedSearchResultsItm)){
				return true;
			}else{
				return false;
			}

} catch (ex){
		nlapiLogExecution('ERROR', 'erp_sdc_UEBL bClosingUnique', ex.toString());
}

}


function getAllDates(){

	var dateObject = new Object();
	var currentDate = new Date();																								//var currentDate = nlapiDateToString(currentDate , 'date');

	nlapiLogExecution('DEBUG', 'currentDate', currentDate);

	var todayDate = moment(currentDate).format('L');
	var weekStart = moment(currentDate).startOf('isoweek').format('L');
	var weekEnd = moment(currentDate).endOf('isoweek').format('L');

	nlapiLogExecution('DEBUG', 'todayDate', todayDate);
	nlapiLogExecution('DEBUG', 'weekStart', weekStart);
	nlapiLogExecution('DEBUG', 'weekEnd', weekEnd);

	var dateRange = getPeriodDateRanges(todayDate);

	dateObject.today = todayDate;
	dateObject.weekStart = weekStart;
	dateObject.weekEnd = weekEnd;

	if(dateRange){
		dateObject.periodStart = moment(dateRange.period.start, ["MM-DD-YYYY", "DD-MM-YYYY", "DD-MMM-YYYY"]).format('L');
		dateObject.periodEnd = moment(dateRange.period.end, ["MM-DD-YYYY", "DD-MM-YYYY", "DD-MMM-YYYY"]).format('L');
		dateObject.qtrStart = moment(dateRange.quarter.start, ["MM-DD-YYYY", "DD-MM-YYYY", "DD-MMM-YYYY"]).format('L');
		dateObject.qtrEnd = moment(dateRange.quarter.end, ["MM-DD-YYYY", "DD-MM-YYYY", "DD-MMM-YYYY"]).format('L');
	}

	nlapiLogExecution('DEBUG', 'periodStart', dateObject.periodStart);
	nlapiLogExecution('DEBUG', 'periodEnd', dateObject.periodEnd);
	nlapiLogExecution('DEBUG', 'qtrStart', dateObject.qtrStart);
	nlapiLogExecution('DEBUG', 'qtrEnd', dateObject.qtrEnd);

	return dateObject;

}


function setAllDates(dateObject){

	//Text dates
	nlapiSetFieldValue('custrecord_day_daterange', dateObject.today);
	nlapiSetFieldValue('custrecord_week_daterange', dateObject.weekStart + ' - ' + dateObject.weekEnd);
	nlapiSetFieldValue('custrecord_period_daterange',  dateObject.periodStart + ' - ' + dateObject.periodEnd);
	nlapiSetFieldValue('custrecord_quarter_daterange',   dateObject.qtrStart + ' - ' + dateObject.qtrEnd);


}

function getShopClosing(locId, date1, date2){


	var salesArrayIds = new Array();
	var salesArrayObj = new Array();

	var salesObject = new Object();

	var strSavedSearchIDShopClosing = null;
	var arSaveSearchFiltersShopClosing = new Array();
	var arSavedSearchColumnsShopClosing = new Array();


	arSaveSearchFiltersShopClosing.push(new nlobjSearchFilter( 'location', null, 'anyof', locId));
	arSaveSearchFiltersShopClosing.push(new nlobjSearchFilter('datecreated', null, 'within', [date1,date2]));

	var arSavedSearchResultsShopClosing = nlapiLoadSearch('transaction', 'customsearch_erp_script_cn_closing');
	arSavedSearchResultsShopClosing.addFilters(arSaveSearchFiltersShopClosing);

	var resultSet = arSavedSearchResultsShopClosing.runSearch();

	try{



		var results = resultSet.getResults(0,1000);

		if(!isNullOrEmpty(results)){

			var idCurrentItem = results[0].getValue('internalid');
			var columns = results[0].getAllColumns();
			var objResult = results[0];

			var objClosingSales = new Object();

			objClosingSales.location = objResult.getValue(columns[0]);
			objClosingSales.closingsales = objResult.getValue(columns[1]);

			nlapiSetFieldValue('custrecord_erp_dclose_netsuite_closing', objClosingSales.closingsales );

			return objClosingSales;


		}

	}catch(ex){
		return null;
	}

}

function getPeriodDateRanges(todayDate){

try{


	var salesArrayIds = new Array();
	var salesArrayObj = new Array();
	var salesObject = new Object();

	var objDateRange = {
			"period": {},
			"quarter": {},
			"year":{},
		};

	var strSavedSearchIDDatePeriod = null;
	var arSaveSearchFiltersDatePeriod = new Array();
	var arSavedSearchColumnsDatePeriod = new Array();
	var arSavedSearchResultsDatePeriod = null;


	var arSavedSearchResultsDatePeriod = nlapiLoadSearch('AccountingPeriod', 'customsearch_erp_accounting_period_2');

	//filters[0] = new nlobjSearchFilter('formulatext', null, 'startswith', 'a');

	//arSaveSearchFiltersDatePeriod.push(new nlobjSearchFilter('formulatext', "case when (    to_date(" + todayDate + ", 'MM/DD/YYYY')      >= {startdate}) then 'true' else 'false' end)", 'is', 'true');
	//arSaveSearchFiltersDatePeriod.push(new nlobjSearchFilter('formulatext', "case when(			to_date(" + todayDate + ", 'MM/DD/YYYY')  		   <= {enddate})   then 'true' else 'false' end)", 'is', 'true');

	arSaveSearchFiltersDatePeriod[0] = new nlobjSearchFilter('formulatext', null, 'is', 'true');
	arSaveSearchFiltersDatePeriod[0].setFormula("case when (    to_date('" + todayDate + "', 'MM/DD/YYYY')      >= {startdate}) then 'true' else 'false' end");

	arSaveSearchFiltersDatePeriod[1] = new nlobjSearchFilter('formulatext', null, 'is', 'true');
	arSaveSearchFiltersDatePeriod[1].setFormula("case when(to_date('" + todayDate + "', 'MM/DD/YYYY')  <= {enddate}) then 'true' else 'false' end");

	arSavedSearchResultsDatePeriod.addColumns(arSavedSearchColumnsDatePeriod);
	arSavedSearchResultsDatePeriod.addFilters(arSaveSearchFiltersDatePeriod);

	var resultSet = arSavedSearchResultsDatePeriod.runSearch();



	if(!isNullOrEmpty(resultSet)){

		var results = resultSet.getResults(0,1000);
		//var objDateRange = new Object();



		if(results){

			for(var i = 0; i < results.length; i++){

				//var idCurrentItem = results[i].getValue('internalid');
				var columns = results[i].getAllColumns();
				var objResult = results[i];


				var year = objResult.getValue(columns[5]);
				var quarter = objResult.getValue(columns[6]);
				var start = objResult.getValue(columns[3]);
				var end = objResult.getValue(columns[4]);
				var thisLoop = '';


				if(year == 'F' && quarter == 'F'){
					thisLoop = 'period';
					objDateRange.period.start= objResult.getValue(columns[3]);
					objDateRange.period.end = objResult.getValue(columns[4]);
				}

				if(year == 'F' && quarter == 'T'){
					thisLoop = 'quarter';
					objDateRange.quarter.start = objResult.getValue(columns[3]);
					objDateRange.quarter.end = objResult.getValue(columns[4]);
				}


				if(year == 'T' && quarter == 'F'){
					thisLoop = 'year';
					objDateRange.year.start = objResult.getValue(columns[3]);
					objDateRange.year.end = objResult.getValue(columns[4]);
				}

//				console.log(thisLoop + ' year:' + year + ' quarter:' + quarter + ' start:' + start + ' end:' + end);


			}

//			console.log(JSON.stringify(objDateRange));
//			nlapiLogExecution('DEBUG', 'DATE RANGE0', JSON.stringify(objDateRange));

	}

		return objDateRange;
}
}catch(ex){}

}






function getAllDates(){

	var dateObject = new Object();
	var currentDate = new Date();																								//var currentDate = nlapiDateToString(currentDate , 'date');

	nlapiLogExecution('DEBUG', 'currentDate', currentDate);

	var todayDate = moment(currentDate).format('L');
	var weekStart = moment(currentDate).startOf('isoweek').format('L');
	var weekEnd = moment(currentDate).endOf('isoweek').format('L');

	nlapiLogExecution('DEBUG', 'todayDate', todayDate);
	nlapiLogExecution('DEBUG', 'weekStart', weekStart);
	nlapiLogExecution('DEBUG', 'weekEnd', weekEnd);

	var dateRange = getPeriodDateRanges(todayDate);

	dateObject.today = todayDate;
	dateObject.weekStart = weekStart;
	dateObject.weekEnd = weekEnd;

	if(dateRange){
		dateObject.periodStart = moment(dateRange.period.start, ["MM-DD-YYYY", "DD-MM-YYYY", "DD-MMM-YYYY"]).format('L');
		dateObject.periodEnd = moment(dateRange.period.end, ["MM-DD-YYYY", "DD-MM-YYYY", "DD-MMM-YYYY"]).format('L');
		dateObject.qtrStart = moment(dateRange.quarter.start, ["MM-DD-YYYY", "DD-MM-YYYY", "DD-MMM-YYYY"]).format('L');
		dateObject.qtrEnd = moment(dateRange.quarter.end, ["MM-DD-YYYY", "DD-MM-YYYY", "DD-MMM-YYYY"]).format('L');
	}

	nlapiLogExecution('DEBUG', 'periodStart', dateObject.periodStart);
	nlapiLogExecution('DEBUG', 'periodEnd', dateObject.periodEnd);
	nlapiLogExecution('DEBUG', 'qtrStart', dateObject.qtrStart);
	nlapiLogExecution('DEBUG', 'qtrEnd', dateObject.qtrEnd);

	return dateObject;

}


function setAllDates(dateObject){

	//Text dates
	nlapiSetFieldValue('custrecord_day_daterange', dateObject.today);
	nlapiSetFieldValue('custrecord_week_daterange', dateObject.weekStart + ' - ' + dateObject.weekEnd);
	nlapiSetFieldValue('custrecord_period_daterange',  dateObject.periodStart + ' - ' + dateObject.periodEnd);
	nlapiSetFieldValue('custrecord_quarter_daterange',   dateObject.qtrStart + ' - ' + dateObject.qtrEnd);


}
