/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       11 April 2017    tim.frazer
 *
 */
 function printTerminalpdf_sl(request, response) {

 try {
           //retrieve the sales order Id passed to the suitelet.
          var id = request.getParameter('id');
            nlapiLogExecution('DEBUG','id:',id);
        //  var id = 6123;
          var record = nlapiLoadRecord('customrecord_erp_shop_daily_closing', id); // load the record to be added
            nlapiLogExecution('DEBUG','record',record);
          var dailyClosing = {};
try{
          dailyClosing.shopName        = record.getFieldValue('custrecord_erp_dclose_shop_name_short');
          dailyClosing.dateCreated     = record.getFieldValue('custrecord_erp_date_created');
          dailyClosing.openingLead     = findEmployeeDailyClosing('employee',record.getFieldValue('custrecord_erp_dclose_opening_lead'));
          dailyClosing.closingLead     = findEmployeeDailyClosing('employee',record.getFieldValue('custrecord_erp_dclose_closing_lead'));
          dailyClosing.shopDirector    = findEmployeeDailyClosing('employee',record.getFieldValue('custrecord_erp_dclose_shop_director'));
          dailyClosing.distDirector    = findEmployeeDailyClosing('employee',record.getFieldValue('custrecord_erp_dclose_district_director'));
          dailyClosing.varianceReason  = record.getFieldValue('custrecord_erp_dclose_variance_reason')||'';
          dailyClosing.monerisTotal,dailyClosing.amountTotal,dailyClosing.varianceTotal;
            nlapiLogExecution('DEBUG','closing notes obj: ',JSON.stringify(dailyClosing));
          dailyClosing.listLen = record.getLineItemCount('recmachcustrecord_cnterm1_link');
}
catch(ex){
  nlapiLogExecution('DEBUG','record',JSON.stringify(dailyClosing));
}

        var xml = '<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">';
          xml += '<pdf onload="javascript:myinit()">';
          xml += '<head><meta name="title" value="Closing Notes Terminal Print"/>';
          xml += '<script> <![CDATA[ function myinit() { this.print(); } ]]> </script> ';
          xml += '<style type="text/css"> table{min-width: 100%;} .term {border-collapse:collapse;border-spacing:0;border-color:#ccc;} .term td{font-family:Verdana, sans-serif;font-size:11px;padding:5px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;border-top-width:1px;border-bottom-width:1px;} .term th{font-family:Verdana, sans-serif;font-size:14px;font-weight:normal;padding:2px 2px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:white;background-color:black;;border-top-width:1px;border-bottom-width:1px;text-transform:uppercase;} .term .term-alt{background-color:#f9f9f9;vertical-align:top}';
          xml +='.header {border-collapse:collapse;border-spacing:0;border-color:#ccc;min-width:100%;align:left;} .header td{font-family:Verdana, sans-serif; font-size:14px;padding:5px 5px; border-style:solid;border-width:0px; overflow:hidden;word-break:normal; border-color:#ccc; color:#333; background-color:#fff; border-top-width:1px; border-bottom-width:1px;} .header .header-alt{background-color:#e3e3e3;}</style>';
          xml += '</head><body padding="10pt 10pt 10pt 10pt" size="Letter-LANDSCAPE">';
          xml += '<br/><br/>';
          xml +='<table class="header"><tr>';
          xml +='<td>SHOWROOM NAME:</td><td class="header-alt">';
          xml +=dailyClosing.shopName;
          xml +='</td>';
          xml +='<td>OPENING LEAD: </td><td class="header-alt">';
          xml +=dailyClosing.openingLead;
          xml +='</td>';
          xml +='<td>SHOP DIRECTOR: </td><td class="header-alt">';
          xml += dailyClosing.shopDirector;
          xml +='</td>';
          xml +='</tr><tr>';
          xml +='<td>DATE: </td><td class="header-alt">';
          xml +=dailyClosing.dateCreated;
          xml +='</td>';
          xml +='<td>CLOSING LEAD: </td><td class="header-alt">';
          xml +=dailyClosing.closingLead;
          xml +='</td>';
          xml +='<td>DISTRICT DIRECTOR: </td><td class="header-alt">';
          xml +=dailyClosing.distDirector;
          xml +='</td>';
          xml +='</tr></table>';
          xml +='<br/>';
          xml += '<table class="header"><tr width="100%"><th width="100%" align="center" class="header-alt">CLOSING DISCREPANCIES</th></tr></table>';
          xml +='<table class="term"><tr><th align="left" height="20" width ="250">Terminal</th><th align="left" height="20" width ="200">Netsuite</th> <th align="left" height="20" width ="200">Moneris $</th> <th align="left" height="20" width ="200">Amount</th> <th align="left" height="20" width ="200">Variance</th> </tr>';

                       for(i = 1; i <= dailyClosing.listLen; i++){
                         xml +='<tr><td align="left" height="20" width ="250">';
                         xml += record.getLineItemValue('recmachcustrecord_cnterm1_link','custrecord_term_id',i);
                         xml +='</td>';

                         xml +='<td align="left" height="20" width ="200">';
                         xml += record.getLineItemValue('recmachcustrecord_cnterm1_link','custrecord_cnterm1_ns',i);
                         xml +='</td>';

                         xml +='<td align="left" height="20" width ="200">';
                         xml += record.getLineItemValue('recmachcustrecord_cnterm1_link','custrecord_cnterm1_moneris',i);
                         dailyClosing.monerisTotal+=  record.getLineItemValue('recmachcustrecord_cnterm1_link','custrecord_cnterm1_moneris',i);
                         xml +='</td>';

                         xml +='<td align="left" height="20" width ="200">';
                         xml += record.getLineItemValue('recmachcustrecord_cnterm1_link','custrecord_cnterm1_amount',i);
                         dailyClosing.amountTotal+= record.getLineItemValue('recmachcustrecord_cnterm1_link','custrecord_cnterm1_amount',i);
                         xml +='</td>';
                         xml +='<td align="left" height="20" width ="200">';
                         xml += record.getLineItemValue('recmachcustrecord_cnterm1_link','custrecord_cnterm1_variance',i);
                         dailyClosing.varianceTotal+= record.getLineItemValue('recmachcustrecord_cnterm1_link','custrecord_cnterm1_variance',i);
                         xml +='</td>';
                        xml +='</tr>';
                        }

                      xml +='</table>';
                      xml += '<table class="header"><tr width="100%"><th width="100%" align="center" class="header-alt">VARIANCE EXPLANATION</th></tr></table>';
                      xml +='<br/>';
                      xml += dailyClosing.varianceReason;
                      xml +='<br/><br/>';
                      xml +='Associate Name/Signature:_______________________________________________';
                      xml +='<br/><br/>';
                      xml +='Lead Name/Signature:_______________________________________________';
                      xml += '</body></pdf>';

          nlapiLogExecution('DEBUG','xml',xml);
           var file = nlapiXMLToPDF(xml);
           nlapiLogExecution('DEBUG','file',file);
           // set content type, file name, and content-disposition
           response.setContentType('PDF', 'terminal_closing.pdf', 'inline');
           // write response to the client
           response.write(file.getValue());
         } catch(exception){
           nlapiLogExecution('DEBUG','exception',file);
         }
 }


function printTerminalpdf_cs() {
  try {
    //call the suitelet
    //console.log('printme');
    var createdPdfUrl = nlapiResolveURL('SUITELET', 'customscript_dailyclosingprintpdf_sl', 'customdeploy_dailyclosingprintpdf_sl', false);
    //pass the internal id of the current record
    createdPdfUrl += '&id=' + nlapiGetRecordId();
    //show the PDF file
    newWindow = window.open(createdPdfUrl);
    }
    catch (exception) {
  }
}




function findEmployeeDailyClosing(listType,id){

    var listValue;
    var col = [];
    col[0] = new nlobjSearchColumn('firstname');
    col[1] = new nlobjSearchColumn('lastname');
    col[2] = new nlobjSearchColumn('internalId');


    var filter = [];

    filter[0] = new nlobjSearchFilter('internalid', null, 'is', id);
    filter[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

    var results = nlapiSearchRecord(listType, null, filter, col);


    if(!isNullOrEmpty(results)){

        if(results.length < 0 && results.length > 1 ){
            return null;
        }
        else{

            var res = results[0];
            listValue = (res.getValue('firstname'));
            listValue +=' ';
            listValue += (res.getValue('lastname'));

            nlapiLogExecution('audit', 'finding list item', JSON.stringify(listValue));

            return listValue;
        }

    }else{

        return null;
    }
}
function isNullOrEmpty(valueStr) {
	return (valueStr === null || valueStr === "" || valueStr === undefined);
}

function createBtnDailyClosing_ue(type, form) {
  //console.log(form);

 if (type == 'view') {
    form.addButton('custpage_Add', 'Print Terminal',"printTerminalpdf_cs()");
    form.setScript('customscript_daily_closing_print_pdf');
  }
}
