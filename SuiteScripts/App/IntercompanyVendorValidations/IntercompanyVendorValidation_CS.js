/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Mar 2017     ivansioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @returns {Boolean} True to continue save, false to abort save
 */
function IntercoVendorValidation_CS_SaveRecord() {

	return true;	
}

function IntercoVendorValidation_CS_ValidateLine(type) {

//	console.log('type ' + type); 
	
	var representSub = nlapiGetFieldValue('representingsubsidiary'); 
	
	if(!isNullOrEmpty(representSub) && type == 'submachine'){
	
		var a = nlapiGetCurrentLineItemValue('submachine','taxitem')
		
		try{
			
			var taxObject = nlapiLoadRecord('salestaxitem', a);
			var thisRate = taxObject.getFieldValue('rate');
			thisRate = parseFloat(thisRate);
			
		//	console.log("thisRate: " + thisRate);
		
			if(thisRate > 0){
				
				alert("you can't use a non-zero taxcode for vendors representing subsidiary"); 
				return false;	
			}else{
				return true;
			}
			
		}catch(ex){
			
			var taxObject = nlapiLoadRecord('taxgroup', a);
			var thisRate = taxObject.getFieldValue('unitprice1');
			var thisRate2 = taxObject.getFieldValue('unitprice2');
			var thisRateTotals = parseFloat(thisRate) + parseFloat(thisRate2);
			
			if(thisRateTotals > 0){
				alert("you can't use a non-zero taxcode for vendors representing subsidiary"); 
				return false;	
			}else{
				return true;
			}
			
		}
	
	}
	return true;
	
}

function IntercoVendorValidation_CS_FieldChanged(type, name, linenum) {
	return true;	
}

function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}