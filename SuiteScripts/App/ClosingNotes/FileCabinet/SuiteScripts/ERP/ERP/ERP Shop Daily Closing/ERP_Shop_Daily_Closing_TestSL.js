/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Jan 2016     ivan.sioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function erp_sdc_SL(request, response){
	
//	var transDate = request.getParameter( 'transactionDate' );
//	var mode = request.getParameter( 'mode' );
//	
//	nlapiLogExecution('DEBUG', '>>>> erp_sdc_SL', 'mode '  +  mode); 
			
	try{
		
		  nlapiLogExecution('DEBUG','this time: START', new Date()); 
		  pause(5); //60 seconds
		  nlapiLogExecution('DEBUG','this time: END', new Date()); 
		  
		
			
			var thisObject = new Object(); 
			thisObject.id = 123;
			thisObject.name = "everybodysaylove";
			
		
//			nlapiLogExecution('DEBUG', '>>>> erp_sdc_SL', 'transDate '  +  transDate); 
			var responseObj = thisObject; //getPeriodDateRanges(transDate); 	    
			response.write( JSON.stringify(responseObj) );					    			
		
	}catch(ex){}
}


function ids_mimic_slowness_bs(){

	  
	  nlapiLogExecution('DEBUG','this time: START', new Date()); 
	  pause(5); //60 seconds
	  nlapiLogExecution('DEBUG','this time: END', new Date()); 
	  
	}


	function pause(waitTime){ //seconds
	    try{
	        var endTime = new Date().getTime() + waitTime * 1000;
	        var now = null;
	        do{
	            //throw in an API call to eat time
	            now = new Date().getTime(); //
	        }while(now < endTime);
	    }catch (e){
	        nlapiLogExecution("ERROR", "not enough sleep");
	    }
	}