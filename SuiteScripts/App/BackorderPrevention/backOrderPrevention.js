//Main Backorder funciton
function mainBackorderScript(type){

	
	var skipECScript = nlapiLookupField('transferorder', nlapiGetRecordId(), 'custbody_erp_skipecscript'); 

	nlapiLogExecution("DEBUG", "context", nlapiGetContext().getExecutionContext() + " | " + skipECScript);

	if (type != 'delete' && skipECScript == 'T'){ // nlapiGetContext().getExecutionContext() == 'suitelet') {


	

	var arrZeroAvgItemObj = new Array();
	var arrZeroAvgItemIds = new Array();
	var backorderTOlines = new Array();
	var removeLines = new Array();

	var errorMsg = false; // Error Message variable that will help us
	var totalCommitted = 0;  //track how may are committed
	var totalItemCount = 0;

	var transactionObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());


	var status = transactionObj.getFieldValue('orderstatus'); 	nlapiLogExecution("DEBUG", "orderstatus", status);
	var form = transactionObj.getFieldValue('customform'); 	nlapiLogExecution("DEBUG", "form", form);

	if ((status == "B") && form ==  147){

			var count = transactionObj.getLineItemCount('item'); var totalcommitted = 0; 
			
			for (var i = 1; i <= count; i++) {
				
				
             	var committed = transactionObj.getLineItemValue('item','quantitycommitted', i); 
             	var lineItem = transactionObj.getLineItemValue('item','item', i);
             	var lineQty = transactionObj.getLineItemValue('item','quantity', i);
                var lineQtyCom = transactionObj.getLineItemValue('item','quantitycommitted', i);
                var lineRate = transactionObj.getLineItemValue('item','rate', i);
                var lineTOMemo = transactionObj.getLineItemValue('item','custcol_erp_to_memo', i);
              	 
              	 if (committed > 0){
                   totalcommitted++; 

//                   nlapiLogExecution("DEBUG", "item[" + i +"] commit", lineQtyCom);
                   
                   transactionObj.setLineItemValue('item', 'custcol_erp_orig_qty', i, lineQty); var lineOrig = transactionObj.getLineItemValue('item','custcol_erp_orig_qty', i);
                   transactionObj.setLineItemValue('item', 'quantity', i, lineQtyCom);
                   transactionObj.setLineItemValue('item', 'custcol_erp_backorder', i, lineOrig - lineQtyCom);

                   
//                   nlapiLogExecution("DEBUG", "item[" + i +"] origqty", lineQty);
                 }
                 else{

                 	var lineObject = new Object;
					lineObject.item = lineItem;
					lineObject.quantity = lineQty;
					lineObject.quantitycommitted = lineQtyCom;
					lineObject.cost = lineRate;
					lineObject.memo = lineTOMemo;

					backorderTOlines.push(lineObject);
                 }
              
            }  

			nlapiLogExecution("AUDIT", "totalcommitted", totalcommitted);	
			
			if(totalcommitted == 0){
				//transactionObj.setFieldValue('orderstatus', 'H'); 
				
				var countk = transactionObj.getLineItemCount('item'); 
				
				for (var k = 1; k <= countk; k++) {
					
					nlapiLogExecution("AUDIT", "closing", k);	
					transactionObj.setLineItemValue('item', 'isclosed', k, 'T');
				}
			}
			else{
				var backorderid = createBackorderTransferOrder(transactionObj, backorderTOlines);
				
			}
			
			transactionObj.setFieldValue('custbody_erp_main_transferorder_number', nlapiGetRecordId());
			transactionObj.setFieldValue('custbody_erp_backorder', backorderid);
			
			
			if(transactionObj.getFieldValue('custbody_erp_disableauto3pl') != 'T'){
				transactionObj.setFieldValue('custbody_export_3pl_status', 1);
			}
			
			nlapiSubmitRecord(transactionObj); 
		}
	
	}

}



function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}


function createBackorderTransferOrder(transferOrderObject, backOrderArray) {

//	Log.d('createBackorderTransferOrder TO OBJ ', JSON
//			.stringify(transferOrderObject));

	try {

		if (!isNullOrEmpty(backOrderArray)) {

//			Log.d('createBackorderTransferOrder backOrderArary ', JSON.stringify(backOrderArray));
//			Log.d('createBackorderTransferOrder backOrderArary ', JSON.stringify(backOrderArray));
//			Log.d('createBackorderTransferOrder backOrderArary ', 'Hey'  + nlapiGetUser());

			var transBack = nlapiCreateRecord('transferorder');

			var thisid = transBack.getId();

			transBack.setFieldValue('location', transferOrderObject.getFieldValue("location"));
			transBack.setFieldValue('orderstatus', 'A');
			transBack.setFieldValue('subsidiary',transferOrderObject.getFieldValue("subsidiary"));
			transBack.setFieldValue('transferlocation',transferOrderObject.getFieldValue("transferlocation"));
			transBack.setFieldValue('custbody_erp_is_backorder', 'T');
			transBack.setFieldValue('custbody_erp_main_transferorder_number',transferOrderObject.getFieldValue("id"));
			transBack.setFieldValue('employee',298465); //TODO Change to parameters
			transBack.setFieldValue('custbody_ka_order_type',transferOrderObject.getFieldValue("custbody_ka_order_type"));

			var newDate = nlapiDateToString(new Date(transferOrderObject.getFieldValue("trandate")));

			transBack.setFieldValue('trandate', newDate);

			for (var i = 0; i < backOrderArray.length; i++) {

				var backOrder = backOrderArray[i];

				transBack.selectNewLineItem("item");

				transBack.setCurrentLineItemValue("item", "item", backOrder.item);
				transBack.setCurrentLineItemValue("item", "quantity", backOrder.quantity);
				transBack.setCurrentLineItemValue("item", "rate", backOrder.cost);
				transBack.setCurrentLineItemValue("item","custcol_erp_to_memo", backOrder.memo);
				transBack.setCurrentLineItemValue("item","custcol_erp_backorder", backOrder.quantity);
				transBack.setCurrentLineItemValue("item","custcol_erp_orig_qty", backOrder.quantity);
				transBack.setCurrentLineItemValue("item","custcol_erp_runbackorder",'T');
				transBack.setCurrentLineItemValue("item", "isclosed", 'T');

				

				transBack.commitLineItem("item");
			}

			var id = nlapiSubmitRecord(transBack);

			return id;
			

		} else {
			return null;
		}

	} catch (ex) {

		
		return null;
	}

}