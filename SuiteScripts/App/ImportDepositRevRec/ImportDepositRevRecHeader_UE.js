/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       06 Dec 2016     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */

function buildDepositLines(objDeposit){

	var paymentcount = objDeposit.getLineItemCount('payment');

	nlapiLogExecution('AUDIT','buildDepositLines count:',paymentcount);
	var arrDeposit = [];

	for(var i = 1 ; i <= paymentcount; i++){

		var depositObj = new Object();
		depositObj.docnumber = objDeposit.getLineItemValue('payment', 'docnumber', i);
		depositObj.type = objDeposit.getLineItemValue('payment', 'type', i);
		depositObj.line= i;

		arrDeposit.push(depositObj);

	}

	nlapiLogExecution('AUDIT','buildDepositLines arrDeposit:',arrDeposit.toString());

	return arrDeposit;

}

function getTransactionRecodType(id){

	var tranID = '';
	switch(id){
		case '1'  : tranID = 'Journal'; break;
		case '2'  : tranID = 'InvTrnfr'; break;
		case '3'  : tranID = 'Check'; break;
		case '5'  : tranID = 'CashSale'; break;
		case '6'  : tranID = 'Estimate'; break;
		case '7'  : tranID = 'CustInvc'; break;
		case '9'  : tranID = 'CustPymt';	break;
		case '10' : tranID = 'CustCred'; break;
		case '11' : tranID = 'InvAdjst'; break;
		case '15' : tranID = 'PurchOrd'; break;
		case '16' : tranID = 'ItemRcpt';	break;
		case '17' : tranID = 'VendBill'; break;
		case '18' : tranID = 'VendPymt'; break;
		case '20' : tranID = 'VendCred'; break;
		case '28' : tranID = 'ExpRept'; break;
		case '29' : tranID = 'CashRfnd';	break;
		case '30' : tranID = 'CustRfnd'; break;
		case '31' : tranID = 'SalesOrd'; break;
		case '32' : tranID = 'ItemShip';	break;
		case '33' : tranID = 'RtnAuth';	break;
		case '34' : tranID = 'Build'; break;
		case '37' : tranID = 'Opprtnty'; break;
		case '40' : tranID = 'CustDep';	break;
		case '42' : tranID = 'binworksheet'; break;
		case '43' : tranID = 'VendAuth'; break;
		case '45' : tranID = 'BinTrnfr';	break;
		case '48' : tranID = 'Transfer'; break;

	}
	return tranID;

}

function createBankDeposit(type){

	nlapiLogExecution('DEBUG', 'type', type);

	var statusId = nlapiGetFieldValue('custrecord_cd_status');

	if( type == 'edit' && statusId != 1){

		try{

			var idSubsidiary = nlapiGetFieldValue('custrecord_cd_subsidiary');
			var dtDate = nlapiGetFieldValue('custrecord_cd_date');
			var idAccount = nlapiGetFieldValue('custrecord_cd_account');
			var idCurrency = nlapiGetFieldValue('custrecord_cd_currency');

			var objDeposit = nlapiCreateRecord("deposit", {account:idAccount}); //disablepaymentfilters: 'true'}
			//objDeposit.setFieldValue("account",idAccount);
			//objDeposit.setFieldValue("subsidiary",idSubsidiary);
			//objDeposit.setFieldValue("trandate",dtDate);


			//nlapiLogExecution('AUDIT','dtDate', dtDate);


			var deposito = buildDepositLines(objDeposit);
			//nlapiLogExecution('AUDIT','deposito', JSON.stringify(deposito));

			var intPaymentCount = nlapiGetLineItemCount('recmachcustrecord_cd_lines_parent');
			nlapiLogExecution('AUDIT','intPaymentCount', intPaymentCount);

			var processDeposit = true;

			for (var j = 1; j <= intPaymentCount; j++){

				var idTranType =  nlapiGetLineItemValue('recmachcustrecord_cd_lines_parent', 'custrecord_cd_line_type', j);
				idTranType = getTransactionRecodType(idTranType);

				var idDocumentNumber =  nlapiGetLineItemValue('recmachcustrecord_cd_lines_parent', 'custrecord_cd_line_docnumber', j);
				var fAmount =  nlapiGetLineItemValue('recmachcustrecord_cd_lines_parent', 'custrecord_cd_line_amount', j);
				fAmount = parseFloat(fAmount);

//				Finding the Payment
				var paymentIndexOne = objDeposit.findLineItemValue('payment','docnumber', idDocumentNumber);

				var depositObject = _(deposito).filter(function(x) {
					  return (x.docnumber == idDocumentNumber && x.type == idTranType); //TODO change to variable
				});

				nlapiLogExecution('AUDIT','paymentIndexOne', paymentIndexOne);
				nlapiLogExecution('AUDIT','depositObject', JSON.stringify(depositObject));
				//If Found
				if(!isNullOrEmpty(depositObject)){

					var paymentIndex = depositObject[0].line; paymentIndex = parseInt(paymentIndex);

					nlapiLogExecution('AUDIT','paymentIndex on JSON [' + j + '][' + idDocumentNumber + '][' + idTranType + '][' + paymentIndex + ']', '[' + JSON.stringify(depositObject) + '][' + paymentIndexOne + ']');

					var fDepAmount = objDeposit.getLineItemValue('payment', 'paymentamount', paymentIndex); fDepAmount = parseFloat(fDepAmount);

					if(fAmount == fDepAmount){
						objDeposit.setLineItemValue('payment', 'deposit', paymentIndex, 'T');
						nlapiSetLineItemValue('recmachcustrecord_cd_lines_parent', 'custrecord_cd_line_memo', j, 'ok - amounts are matching');
						nlapiSetLineItemValue('recmachcustrecord_cd_lines_parent', 'custrecord_cd_line_status', j, 1);
						
						
					}else{
						nlapiSetLineItemValue('recmachcustrecord_cd_lines_parent', 'custrecord_cd_line_memo', j, 'amounts are not matching');
						nlapiSetLineItemValue('recmachcustrecord_cd_lines_parent', 'custrecord_cd_line_status', j, 2);
						processDeposit = false;
					}

				//If Not Found
				}else{
					nlapiSetLineItemValue('recmachcustrecord_cd_lines_parent', 'custrecord_cd_line_memo', j, 'document number not found');
					nlapiSetLineItemValue('recmachcustrecord_cd_lines_parent', 'custrecord_cd_line_status', j, 2);
					processDeposit = false;
				}

			}


			nlapiLogExecution('AUDIT','processDeposit', processDeposit);

			if(!processDeposit){

				nlapiSetFieldValue('custrecord_cd_status', 2);
				nlapiSetFieldValue('custrecord_cd_memo', 'deposit could not be created, see transaction lines for details');
			}else{

				var depositId = nlapiSubmitRecord(objDeposit);
				nlapiLogExecution('AUDIT','deposit record', depositId);

				if(!isNullOrEmpty(depositId)){
					nlapiSetFieldValue('custrecord_cd_memo', 'deposit successful');
					nlapiSetFieldValue('custrecord_cd_status', 1);
					nlapiSetFieldValue('custrecord_cd_deposit', depositId);
				}else{
					nlapiSetFieldValue('custrecord_cd_memo', 'deposit could not be created, see transaction lines for details');
					nlapiSetFieldValue('custrecord_cd_status', 2);

				}
			}


		}
		catch(ex){

			nlapiLogExecution('AUDIT','ex', ex.toString());
			nlapiSetFieldValue('custrecord_cd_memo', ex.toString());

		}

	}

}

function getExistingRecordViaExternalID(strCustomRecordID, strExternalID)
{
	var custRecord = null;
	try
	{
		var processMssg = 'searching for existing record via external id.';

		var searchFilters = [ new nlobjSearchFilter('externalid', null, 'is', strExternalID) ];
		var searchColumns = [ new nlobjSearchColumn('internalid') ];

		var results = nlapiSearchRecord(strCustomRecordID, null, searchFilters, searchColumns);

		if (!isNullOrEmpty(results))
		{
			objResult = results[0];

			intInternalID = objResult.getId();// getValue('internalid');
			return intInternalID;
		}

	} catch (ex)
	{
		var errorStr = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() + '\n' : ex.toString();
		nlapiLogExecution('Error', 'processManualTask()', 'Error encountered while ' + processMssg + ' == ' + errorStr);
	}
	return custRecord;


}

function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}
