/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Dec 2016     ivansioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function createDeposits(request, response){

	var thisEmployeeId = request.getParameter('employee');
	
	nlapiLogExecution('AUDIT','thisEmployeeId', thisEmployeeId);
  
var objDeposit = nlapiCreateRecord("deposit", {account:'488'});
var paymentcount = objDeposit.getLineItemCount('payment');


	nlapiLogExecution('AUDIT','buildDepositLines paymentcount:', paymentcount);
  

	searchIncompleteTransferStaging(thisEmployeeId);
	response.write('done'); 
	
}

function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}

function searchIncompleteTransferStaging(employeeId){
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_cd_status', 	 null, 'anyof',  3)); //pending
	filters.push(new nlobjSearchFilter('custrecord_cd_deposit', null, 'anyof', '@NONE@'));   
	filters.push(new nlobjSearchFilter('owner', null, 'anyof', employeeId));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));       
	//add filter for the employee
	
	var cols = new Array();
	cols.push(new nlobjSearchColumn('internalid').setSort());
	
	var searchResult = nlapiSearchRecord('customrecord_create_deposit', null, filters, cols);
	//console.log(searchResult.length);
	nlapiLogExecution('AUDIT', 'searchResult', searchResult);
	
	if(!isNullOrEmpty(searchResult)){
		
		nlapiLogExecution('AUDIT', 'searchResult.length', searchResult.length);
		
		for(var i = 0 ; i < searchResult.length; i++){
			var c = searchResult[i].getValue('internalid');
			
			nlapiLogExecution('AUDIT', 'Transfer Staging', c);
			try{
				var a = nlapiLoadRecord('customrecord_create_deposit', c);
				a.setFieldValue('custrecord_cd_status', 3);
				var d = nlapiSubmitRecord(a); 
				
				nlapiLogExecution('AUDIT', 'nlapiSubmitRecord', d);
				
//				var transnum = nlapiLookupField('customrecord_erp_transferorder_staging', d, 'custrecord_erp_toi_transferref');
				
				//add the suitelet here
				//transactionRecord
				
//				TEMP COMMENT and encorporating the script in the UE
//				var resolveUrl = nlapiResolveURL('SUITELET','customscript_erp_create_to_import_sl','customdeploy_erp_create_to_import_sl', true);  
//				
//				var slCall = new Array();		
//				slCall['User-Agent-x'] = 'SuiteScript-Call';
//			    
//			    var params = [];
//				params['transactionRecord'] = transnum; 
//				
//				
//				nlapiRequestURL(resolveUrl, params, slCall); 
				
			}
			catch(ex){
				//
				if(ex.code != 'SSS_REQUEST_TIME_EXCEEDED'){				
					nlapiSubmitField('customrecord_create_deposit', c,  'custrecord_cd_status', 2);
					nlapiSubmitField('customrecord_create_deposit', c,  'custrecord_cd_memo', ex.toString());
				}
				else{
					nlapiSubmitField('customrecord_create_deposit', c,  'custrecord_cd_memo', 'warning: transfer order would take time due to the transfer order size');
				}
			}
			//nlapiSubmitField('customrecord_erp_transferorder_staging', c,  'custrecord_transorder_created_stat', 1);
			//console.log(d);
		}
	}
	
	
}
