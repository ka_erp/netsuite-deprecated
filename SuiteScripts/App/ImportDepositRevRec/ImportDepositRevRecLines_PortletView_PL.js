/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Jan 2017     ivansioson
 *
 */

/**
 * @param {nlobjPortlet} portletObj Current portlet object
 * @param {Number} column Column position index: 1 = left, 2 = middle, 3 = right
 * @returns {Void}
 */
function onStart(portlet, column)
{
  var suiteletURL = SCRIPTCONFIG.getScriptConfigValue('Deposit Import: Portlet');
  
  
    suiteletURL = suiteletURL + "&user=" + nlapiGetUser();
    
    nlapiLogExecution('AUDIT','This User', nlapiGetUser());

    
    portlet.setTitle('Deposit Import');
    portlet.setHtml('<html><div><iframe src="' + suiteletURL + '" scrolling="auto" width="100%" height="250px"></iframe></div></html>');
}
