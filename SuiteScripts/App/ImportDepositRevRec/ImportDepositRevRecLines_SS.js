/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Dec 2016     ivansioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function runDepositSched() //945 call from Cloudhub for TO processing
{
	nlapiLogExecution('AUDIT', 'START BEGIN', new Date());

	
	var context = nlapiGetContext();
    var fileID = context.getSetting("SCRIPT", "custscript_idrrl_fileid");
    
    
    
    
	nlapiLogExecution('AUDIT', 'fileID', fileID);
	nlapiLogExecution('AUDIT', 'userId', nlapiGetUser());

	var import2 = nlapiCreateCSVImport();
	import2.setMapping('CUSTIMPORTDEPOSITIMPORT');
	
	var file = nlapiLoadFile(fileID); 
	import2.setPrimaryFile(file); //internal id of first csv file
	
	
	
	try{
		var c = nlapiSubmitCSVImport(import2); var d = parseInt(c);	
		
		if(c.indexOf("No empty or blank") >= 0 ){
			nlapiLogExecution('AUDIT', 'Running Email NOW', 'Sending');
			var a = "Please fix the file and reprocess. Errors below : \n\n"; a = a + c;
			nlapiSendEmail(nlapiGetUser(), nlapiGetUser(), 'Error on CSV : ' + file.getName(), a , null, null, null, null);s
			
		}
		
		nlapiLogExecution('AUDIT', 'END ', c);
	}catch(ex){
	
	
		nlapiLogExecution('AUDIT', 'Running Email START', ex.toString());
		if(ex.toString().indexOf("No empty or blank") >= 0 ){
			nlapiLogExecution('AUDIT', 'Running Email NOW', 'Sending');
			nlapiSendEmail(nlapiGetUser(), nlapiGetUser(), 'Error on CSV : ' + file.getName(), c , null, null, null, null);
			
		}
		nlapiLogExecution('AUDIT', 'END ', c);
	}
	
	//nlapiLogExecution('AUDIT', 'Running Email END', nlapiGetUser());
			

}