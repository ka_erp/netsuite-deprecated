/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Dec 2016     ivansioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */

function findItemByUPC(upccode) {

	var itemid = "";
	var filters = new Array();
	filters.push(new nlobjSearchFilter('upccode', null, 'is', upccode));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var cols = new Array();
	cols.push(new nlobjSearchColumn('internalid'));

	var searchResult = nlapiSearchRecord('inventoryitem', null, filters, cols);

	if (!isNullOrEmpty(searchResult)) {
		itemid = searchResult[0].getId();
	}

	return itemid;

}

function createParent_UE_BS(type) {

	nlapiLogExecution('AUDIT', 'Parent EXT ID', 'forced end message ');
	var thisEmployeeId = nlapiGetUser(); //nlapiGetFieldValue('custrecord_toil_employee'); 

	var thisParentExtId = nlapiGetFieldValue('custrecord_cd_line_parent_name');
	
	if (thisParentExtId == "END") {

		var url = nlapiResolveURL('SUITELET', 'customscript_createdeposit_sl',
				'customdeploy_createdeposit_sl', true);

		var params = new Array();
		params['employee'] = thisEmployeeId;

		nlapiLogExecution('AUDIT', 'Parent EXT ID [' + thisEmployeeId + ']',
				'forced end message ' + url);

		var slResponse = nlapiRequestURL(url, params);

		nlapiLogExecution('AUDIT', 'Parent EXT ID', 'forced end message '
				+ slResponse);
		
		throw nlapiCreateError('this is the end', 'forced end message');

	} else {

		
		nlapiLogExecution('DEBUG', 'getting parent name from lines',
				thisParentExtId);

		var parentExtId = getExistingRecordViaExternalID(
				'customrecord_create_deposit', thisParentExtId);
		var employeeId = nlapiGetUser(); //nlapiGetFieldValue('custrecord_toil_employee'); 

		try {

			//PARENT
			if (parentExtId != null) {

				if(parentExtId.inactive != 'T'){
					nlapiLogExecution('DEBUG', 'parentExtId', parentExtId.id);
					nlapiSetFieldValue('custrecord_cd_lines_parent', parentExtId.id);
				}else{
					
					nlapiSetFieldValue('custrecord_cd_line_memo', 'the parent has been created and inactivated. line is orphan');
					nlapiSetFieldValue('custrecord_cd_line_status', 2); //Error
				}

			} else {
				
						nlapiLogExecution('DEBUG', 'creating the new parent record',thisParentExtId);
		
						var newParent = nlapiCreateRecord('customrecord_create_deposit');
						newParent.setFieldValue('externalid', thisParentExtId);
						newParent.setFieldValue('name', thisParentExtId);
		
						var accountId = nlapiGetFieldValue('custrecord_cd_line_account');
						
						if(isNullOrEmpty(accountId)){
							var accountIdExt = nlapiGetFieldValue('custrecord_cd_line_accounextid');
							accountObj = getExistingRecordViaExternalID('account', accountIdExt) 
						}
						
		
						if (!isNullOrEmpty(accountObj)) {
		
							if(accountObj.inactive != "T"){
							
							var subId = nlapiLookupField('account', accountObj.id,'subsidiary');
							newParent.setFieldValue('custrecord_cd_account', accountObj.id);
							newParent.setFieldValue('custrecord_cd_subsidiary', subId);
							
							}else{
								
								nlapiSetFieldValue('custrecord_cd_line_memo', 'the account has been created and inactivated.' );
								nlapiSetFieldValue('custrecord_cd_line_status', 2); //Error
							}
		
						}
		
						var newParentExtId = nlapiSubmitRecord(newParent);
						nlapiLogExecution('DEBUG', 'created the new parent',
								parentExtId);
					
		
					nlapiLogExecution('DEBUG', 'using parentExtId', newParentExtId);
		
					nlapiSetFieldValue('custrecord_cd_lines_parent', newParentExtId);
					
			

		} 
		
		}catch (ex) {

			var errorStr = (ex.getCode != null) ? ex.getCode() + '\n'
					+ ex.getDetails() + '\n' : ex.toString();
			nlapiLogExecution('Error', 'createParent_UE_BS()', errorStr);
		}
	}

}

function getExistingRecordViaExternalID(strCustomRecordID, strExternalID) {
	var custRecord = null;
	var processMssg = 'searching for existing record via external id.';
	try {

		var searchFilters = [ new nlobjSearchFilter('externalid', null, 'is',
				strExternalID) ];
		var searchColumns = [ ];
		
		searchColumns.push( new nlobjSearchColumn('internalid'));
		searchColumns.push( new nlobjSearchColumn('isinactive'));

		var results = nlapiSearchRecord(strCustomRecordID, null, searchFilters,
				searchColumns);

		//		nlapiLogExecution('DEBUG','results', results[0].getId());

		if (!isNullOrEmpty(results)) {
			objResult = results[0];
			//			intInternalID = objResult.getId();// getValue('internalid');
			//			if (!isNullOrEmpty(intInternalID))
			//			{
			//				custRecord = nlapiLoadRecord(strCustomRecordID, intInternalID);
			//			}

			intInternalID = objResult.getId();// getValue('internalid');
			bInactive = objResult.getValue('isinactive'); // getValue('internalid')
			
			var objObject = new Object();
			
			objObject.id = intInternalID;
			objObject.inactive = bInactive; 
			
			return objObject;
		}

	} catch (ex) {
		var errorStr = (ex.getCode != null) ? ex.getCode() + '\n'
				+ ex.getDetails() + '\n' : ex.toString();
		nlapiLogExecution('Error', 'processManualTask()',
				'Error encountered while ' + processMssg + ' == ' + errorStr);
	}
	return custRecord;

}

function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}