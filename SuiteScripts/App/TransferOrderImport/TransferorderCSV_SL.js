/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Dec 2016     ivansioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	
	
	nlapiLogExecution('AUDIT', 'START', new Date());
	
	var import2 = nlapiCreateCSVImport();
	import2.setMapping('CUSTIMPORTTransferOrderImport');
	import2.setPrimaryFile(nlapiLoadFile(7421343)); //internal id of first csv file
	import2.setQueue('1'); // run in queue 2
	var c = nlapiSubmitCSVImport(import2);
	
	nlapiLogExecution('AUDIT', 'ID', c);
	
	nlapiLogExecution('AUDIT', 'END', new Date());

}
