/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Jan 2017     ivansioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

var InputData = {
    FileID:undefined,
    User:undefined
};



function TransferOrderImporter(request, response)
{
	
	
	var thismethod = request.getMethod(); 
	nlapiLogExecution('AUDIT','Method', thismethod);
    // On GET display the form to the user
    if (request.getMethod() == 'GET')
    {
        var form = nlapiCreateForm('Transfer Order Input', false);

        var fileField = form.addField('file', 'file', 'Import File', null, null);
        fileField.setMandatory(true);

        // Set a custom User field so we can track who imported this file
        var loggedInUser = nlapiGetUser();  request.getParameter('user');
        //Log.d("FORM GET - Show Form", "loggedInUser:  " + loggedInUser);
        var userField = form.addField('user', 'text', 'User', null, null);
        userField.setDefaultValue(loggedInUser);
        userField.setDisplayType("hidden");

        form.addSubmitButton("Process Transfer Orders");
        response.writePage(form);
    }
    else if (request.getMethod() == 'POST')
    {
        try {
//            //Log.d("TransferOrderImporter - FORM SUBMITTED", "POST CHECK 1");
        	//nlapiLogExecution('AUDIT','Method : 2', thismethod);

        	
        	//var InputData = new Object(); 
            InputData.User = request.getParameter('user');
            
            nlapiLogExecution('AUDIT','Function', JSON.stringify(InputData)); 
            
//            //Log.d("TransferOrderImporter", "PARAMETER - User:  " + InputData.User);
//
            var file = request.getFile("file");
//            //Log.d("TransferOrderImporter - FORM SUBMITTED", "File Retrieved");
//
//            // Validate the uploaded File
            if (!file) throw nlapiCreateError("NO_FILE_UPLOADED", "You must upload a file.");
//            //Log.d("TransferOrderImporter - FORM SUBMITTED", "File Name:  " + file.getName());
            if (file.getName()) {
                var fileNameParts = file.getName().split(".");
                var extension = fileNameParts[1];
                nlapiLogExecution('AUDIT','fileNameParts' + fileNameParts, "TransferOrderImporter - FORM SUBMITTED", "extension:  " + extension);
                extension = extension.toLowerCase();
                if (extension != "csv") throw nlapiCreateError("INVALID_FILE_TYPE", "Only csv files are permitted for upload.");
            }
//
//            // Save this file to the File Cabinet
            file.setName(file.getName());
//            
            
            var FILES_FOLDER = SCRIPTCONFIG.getScriptConfigValue('Transfer Order Import: Folder');
            file.setFolder(FILES_FOLDER);
            InputData.FileID = nlapiSubmitFile(file);
            
            
            nlapiLogExecution('AUDIT','submit', InputData.FileID);
            
            
            
        	var headers 				= new Array();
        	headers['Content-Type']  	= 'application/json';
        	headers['Accept']  		 	= 'application/json';
//        	headers['Authorization'] 	= 'NLAuth nlauth_account=' + nlapiGetContext().getCompany() + ',nlauth_email=ivan.sioson@kitandace.com,nlauth_signature=M@nila120!84,nlauth_role=3';

        	var stAuth = SCRIPTCONFIG.getScriptConfigValue('Transfer Order Import: Access');
        	if(stAuth){
        		var objAuth = JSON.parse(stAuth);
        		headers['Authorization'] = 'NLAuth nlauth_account=' + nlapiGetContext().getCompany()
        			+ ',nlauth_email=' + objAuth.email
        			+ ',nlauth_signature=' + objAuth.pwd
        			+ ',nlauth_role=' + objAuth.role;
        	}
        	
        	
            var orderRequest 			= new Object();
			orderRequest.id  			= InputData.FileID; 
			orderRequest.user  			= InputData.User; 
			
			nlapiLogExecution('AUDIT','submit', 'before call: ' + JSON.stringify(orderRequest));
			
			var url = SCRIPTCONFIG.getScriptConfigValue('Transfer Order Import: Restlet');
			
			
			  nlapiLogExecution('AUDIT','restresponse' , 'pre call: ' + url );
			  nlapiLogExecution('AUDIT','restresponse' , 'pre call: ' + JSON.stringify(orderRequest));
			  nlapiLogExecution('AUDIT','restresponse' , 'pre call: ' + headers.toString() );
			
            var restresponse = nlapiRequestURL(url, JSON.stringify(orderRequest), headers);
           
            
            nlapiLogExecution('AUDIT','submit' , 'after call: ' + restresponse.code );
            
            
         
            
            //nlapiSetRedirectURL( 'SUITELET', 'customscript_transferorderimport_sl', 'customdeploy_transferorderimport_sl', null, null );
//            
            if(restresponse.code == 400){
            	
            	var fhform = nlapiCreateForm('Error', false);
            	
                var html = '<br /><br />' +
                '<span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; ' +
                'color: #333333;">';
	            html += 'An unexpected error occurred during upload.  Error details are below and will be emailed to you.  Contact Support if you continue to have issues submitting files through this portlet.</span><br /><br />';
	            html += 'Error Details:<br />';
	            html += 'CODE:  ' + restresponse.code + '<br />';
	            html += 'DETAILS:  ' + restresponse.error;
	            html += '<br /><br />';
            	
                var field = fhform.addField('thankyou','inlinehtml', 'thankyou');
                field.setDefaultValue(html);
            	
            }else{
            	var fhform = nlapiCreateForm('Thank you', false);
            	
                var html = '<br /><br />' +
                '<span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; ' +
                'color: #333333;">';
	            html += 'Thank you</span><br /><br />';
	            html += '<a href="https://system.sandbox.netsuite.com/app/setup/upload/csv/csvstatus.nl?whence=" target="_blank"> Check CSV Status </a><br />';
	            html += '<a href="https://system.sandbox.netsuite.com/app/common/custom/custrecordentrylist.nl?rectype=407&searchtype=Custom&searchid=1942&refresh=&whence=" target="_blank"> Check Transfer Order Staging</a><br />';
	            html += '<br /><br />';
            	
                var field = fhform.addField('thankyou','inlinehtml', 'thankyou');
                field.setDefaultValue(html);
            	
            }
            
            response.writePage(fhform);
//            
//            response.writePage(form);
            
        }
        catch(e)
        {
        	nlapiLogExecution('AUDIT','ERROR', e.toString());
        }
    }
}