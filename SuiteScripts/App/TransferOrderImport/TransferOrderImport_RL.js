/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Dec 2016     ivansioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function postRunFile(dataIn) //945 call from Cloudhub for TO processing
{
	nlapiLogExecution('AUDIT', 'START BEGIN', new Date());
	
	if(dataIn != null){
		
//		try{
			nlapiLogExecution('AUDIT','Testing The File',nlapiGetUser());

			nlapiLogExecution('AUDIT', 'START' + new Date(),  JSON.stringify(dataIn));
			
			var import2 = nlapiCreateCSVImport();
			import2.setMapping('CUSTIMPORTTransferOrderImport');
			
			nlapiLogExecution('AUDIT', 'dataIn.id', dataIn.id);
			import2.setPrimaryFile(nlapiLoadFile(dataIn.id)); //internal id of first csv file
//			import2.setOption('jobName','testing'); 
////			import2.setQueue('1'); // run in queue 2
			
			
			var c = nlapiSubmitCSVImport(import2); var d = parseInt(c);
			
			nlapiLogExecution('AUDIT', 'END ', c);
			
			if(isNaN(d)){ throw nlapiCreateError('ERROR', c)}
			
			
			
//		}catch(ex){
//			
//			 ex.toString();
//		}
	
	}
	
	return c;

}