/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Dec 2016     ivansioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */

function findItemByUPC(upccode){
	
	var itemid = "";
	var filters = new Array(); 
	filters.push(new nlobjSearchFilter('upccode', null, 'is', upccode)); 
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F')); 

	var cols = new Array();
	cols.push(new nlobjSearchColumn('internalid'));

	var searchResult = nlapiSearchRecord('inventoryitem', null, filters, cols);
	
	if(!isNullOrEmpty(searchResult)){
		itemid = searchResult[0].getId(); 
	}
	
	return itemid; 
	
}

function createParent_UE_BS(type){
	
	var thisParentExtId = nlapiGetFieldValue('custrecord_erp_parent_extid'); 
	var thisEmployeeId = nlapiGetFieldValue('custrecord_toil_employee'); 
	
	nlapiLogExecution('DEBUG', 'Parent EXT ID', thisParentExtId); 
	if (thisParentExtId == "END"){
		
		var url = nlapiResolveURL('SUITELET',
				'customscript_transorderimp_sl',
				'customdeploy_transorderimp_sl', true);

			var params = new Array();
			params['employee'] = thisEmployeeId;

			nlapiLogExecution('AUDIT','Parent EXT ID [' + thisEmployeeId + ']',  'forced end message ' + url);	
			
			var slResponse = nlapiRequestURL(url, params);
		
		
		nlapiLogExecution('AUDIT','Parent EXT ID',  'forced end message ' + slResponse);
		throw nlapiCreateError('this is the end', 'forced end message fsasf1@#!');
		
	}else{
	
		
		var parentExtId = getExistingRecordViaExternalID('customrecord_erp_transferorder_staging', thisParentExtId);
		var fromLocaitonId = getExistingRecordViaExternalID('location', nlapiGetFieldValue('custrecord_toil_fromlocation_txt'));
		var toLocationId = getExistingRecordViaExternalID('location', nlapiGetFieldValue('custrecord_toil_tolocation_txt'));
		var employeeId = nlapiGetFieldValue('custrecord_toil_employee'); 
			
		nlapiSetFieldValue('custrecord_toil_tolocation', toLocationId);
		nlapiSetFieldValue('custrecord_toil_fromlocation', fromLocaitonId);
		
		
		//GET THE INTERNAL ID of the Item, and used to reference the Item
		var upccode = nlapiGetFieldValue('custrecord_toil_item_upc');
		
		if(!isNullOrEmpty(upccode)){
			
			upccode = findItemByUPC(upccode); nlapiLogExecution('AUDIT','upccode', upccode);	
			if(!isNullOrEmpty(upccode)){
				nlapiSetFieldValue('custrecord_transorderline_item', upccode);
			
			}	else{
				nlapiSetFieldValue('custrecord_transorderline_item', '');
				nlapiSetFieldValue('custrecord_toil_memo', 'cannot find the upc');
			}
						
		}
		
		
		if(parentExtId != null){
			
			nlapiLogExecution('DEBUG','parentExtId', parentExtId);	
			nlapiSetFieldValue('custrecord_transorderline_link', parentExtId);
			
		}else{
			
			var newParent = nlapiCreateRecord('customrecord_erp_transferorder_staging');
			newParent.setFieldValue('externalid', thisParentExtId);
			
			newParent.setFieldValue('name', thisParentExtId);
			
			newParent.setFieldValue('custrecord_toi_employee', employeeId);
			newParent.setFieldValue('custrecord_erp_toi_fromloc', fromLocaitonId);
			newParent.setFieldValue('custrecord_erp_toi_tolocation', toLocationId);
			newParent.setFieldValue('custrecord_erp_toi_subsidiary', nlapiGetFieldValue('custrecord_toil_subsidiary'));
			newParent.setFieldValue('custrecord_toi_transfertype', nlapiGetFieldValue('custrecord_toil_transfertype'));
			newParent.setFieldValue('custrecord_toi_expectedshipdt', nlapiGetFieldValue('custrecord_toil_expectedshipdt'));
			newParent.setFieldValue('custrecord_toi_expectedrecvdt', nlapiGetFieldValue('custrecord_toil_expectedrecvdt'));
			
			parentExtId =  nlapiSubmitRecord(newParent);
		}
		
		nlapiSetFieldValue('custrecord_transorderline_link', parentExtId);
	}
 
}

function getExistingRecordViaExternalID(strCustomRecordID, strExternalID)
{
	var custRecord = null;
	try
	{
		var processMssg = 'searching for existing record via external id.';

		var searchFilters = [ new nlobjSearchFilter('externalid', null, 'is', strExternalID) ];
		var searchColumns = [ new nlobjSearchColumn('internalid') ];

		var results = nlapiSearchRecord(strCustomRecordID, null, searchFilters, searchColumns);
		
		nlapiLogExecution('DEBUG','results', results[0].getId());

		if (!isNullOrEmpty(results))
		{
			objResult = results[0];
//			intInternalID = objResult.getId();// getValue('internalid');
//			if (!isNullOrEmpty(intInternalID))
//			{
//				custRecord = nlapiLoadRecord(strCustomRecordID, intInternalID);
//			}
			
			intInternalID = objResult.getId();// getValue('internalid');
			return intInternalID;
		}

	} catch (ex)
	{
		var errorStr = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() + '\n' : ex.toString();
		nlapiLogExecution('Error', 'processManualTask()', 'Error encountered while ' + processMssg + ' == ' + errorStr);
	}
	return custRecord;
	
	
}


function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}