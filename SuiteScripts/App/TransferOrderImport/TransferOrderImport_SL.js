/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Dec 2016     ivansioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function createTransferOrders(request, response){

	var thisEmployeeId = request.getParameter('employee');
	
	nlapiLogExecution('AUDIT','thisEmployeeId', thisEmployeeId);

	
	searchIncompleteTransferStaging(thisEmployeeId);
	response.write('done'); 
	
}

function searchIncompleteTransferStaging(employeeId){
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_transorder_created_stat', 	 null, 'anyof',  3)); 
	filters.push(new nlobjSearchFilter('custrecord_erp_toi_transferref', null, 'anyof', '@NONE@'));   
	filters.push(new nlobjSearchFilter('custrecord_toi_employee', null, 'anyof', employeeId));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));       
	//add filter for the employee
	
	var cols = new Array();
	cols.push(new nlobjSearchColumn('internalid').setSort());
	
	var searchResult = nlapiSearchRecord('customrecord_erp_transferorder_staging', null, filters, cols);
	//console.log(searchResult.length);
	
	if(searchResult){
		for(var i = 0 ; i < searchResult.length; i++){
			var c = searchResult[i].getValue('internalid');
			
			nlapiLogExecution('AUDIT', 'Transfer Staging', c);
			try{
				var a = nlapiLoadRecord('customrecord_erp_transferorder_staging', c);
				a.setFieldValue('custrecord_transorder_created_stat', 1);
				var d = nlapiSubmitRecord(a); 
				
				var transnum = nlapiLookupField('customrecord_erp_transferorder_staging', d, 'custrecord_erp_toi_transferref');
				
				//add the suitelet here
				//transactionRecord
				
//				TEMP COMMENT and encorporating the script in the UE
//				var resolveUrl = nlapiResolveURL('SUITELET','customscript_erp_create_to_import_sl','customdeploy_erp_create_to_import_sl', true);  
//				
//				var slCall = new Array();		
//				slCall['User-Agent-x'] = 'SuiteScript-Call';
//			    
//			    var params = [];
//				params['transactionRecord'] = transnum; 
//				
//				
//				nlapiRequestURL(resolveUrl, params, slCall); 
				
			}
			catch(ex){
				//
				if(ex.code != 'SSS_REQUEST_TIME_EXCEEDED'){				
					nlapiSubmitField('customrecord_erp_transferorder_staging', c,  'custrecord_transorder_created_stat', 2);
					nlapiSubmitField('customrecord_erp_transferorder_staging', c,  'custrecord_transord_memo', ex.toString());
				}
				else{
					nlapiSubmitField('customrecord_erp_transferorder_staging', c,  'custrecord_transord_memo', 'warning: transfer order would take time due to the transfer order size');
				}
			}
			//nlapiSubmitField('customrecord_erp_transferorder_staging', c,  'custrecord_transorder_created_stat', 1);
			//console.log(d);
		}
	}
	
	
}
