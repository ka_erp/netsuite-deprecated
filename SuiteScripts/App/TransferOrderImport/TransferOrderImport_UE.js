/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Dec 2016     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function createTransferOrder_UE_BS(type){
 
	nlapiLogExecution('DEBUG', 'type', type); 
	if( type == 'edit'){
		
		nlapiLogExecution('DEBUG', 'got here', 'why are you here'); 
		
		var idSubsidiary = nlapiGetFieldValue('custrecord_erp_toi_subsidiary');
		var idFromLocation = nlapiGetFieldValue('custrecord_erp_toi_fromloc');
		var idToLocation = nlapiGetFieldValue('custrecord_erp_toi_tolocation');
		var idTransferOrderType = nlapiGetFieldValue('custrecord_toi_transfertype');
		
		
		var idItem = nlapiGetFieldValue('custrecord_erp_toi_item');		
			
		var transferOrder = nlapiCreateRecord('transferorder');
		
		
		var intItemCount = nlapiGetLineItemCount('recmachcustrecord_transorderline_link');
		nlapiLogExecution('AUDIT', 'Count', intItemCount);

		transferOrder.setFieldValue('customform', 147); // TODO: Put this in a parameter
		transferOrder.setFieldValue('subsidiary', idSubsidiary); 
		transferOrder.setFieldValue('location', idFromLocation); 
		transferOrder.setFieldValue('transferlocation', idToLocation); 
		transferOrder.setFieldValue('employee',nlapiGetFieldValue('custrecord_toi_employee'));
		transferOrder.setFieldValue('custbody_ka_order_type', idTransferOrderType);	
		
		transferOrder.setFieldValue('custbody_ka_to_expected_ship_date', nlapiGetFieldValue('custrecord_toi_expectedshipdt')); //'A');
		transferOrder.setFieldValue('custbody_ka_to_expected_recv_date', nlapiGetFieldValue('custrecord_toi_expectedrecvdt')); //'A');
		transferOrder.setFieldValue('custbody_erp_to_staging_link', nlapiGetRecordId());
		transferOrder.setFieldValue('memo', nlapiGetFieldValue('name'));
		
		var itemsArray = []; 
		
		for (var j = 1; j <= intItemCount; j++){
			
			var a =  nlapiGetLineItemValue('recmachcustrecord_transorderline_link', 'custrecord_transorderline_item', j);
			var qty =  nlapiGetLineItemValue('recmachcustrecord_transorderline_link', 'custrecord_toil_quantity', j);
			var amount =  nlapiGetLineItemValue('recmachcustrecord_transorderline_link', 'custrecord_transordline_amount', j);
			
			if(!isNullOrEmpty(a)){
				transferOrder.selectNewLineItem('item');
				transferOrder.setCurrentLineItemValue('item', 'item', a);
				transferOrder.setCurrentLineItemValue('item', 'quantity', qty);
				transferOrder.setCurrentLineItemValue('item', 'rate', 0);	
				transferOrder.commitLineItem('item', true);
				
				itemsArray.push( {"id":a}); 
			}
			
		}
		
		var uniqueItems = _.pluck(itemsArray, 'id'); 

		
		try{
			var itemPrices = getItemPrices(idSubsidiary, idFromLocation, uniqueItems); 		//Change to From Location / Source
			transferOrder = setItemPrices(itemPrices, transferOrder, [idFromLocation]); 	//Change to From Location / Source
		}catch(ex){
			nlapiLogExecution('ERROR', 'error out on', ex.toString());
		}
			
		var a = nlapiSubmitRecord(transferOrder);
		nlapiLogExecution('DEBUG', 'this new to', a);
		nlapiSetFieldValue('custrecord_erp_toi_transferref', a);
			
		try{	
			
			if(!isNullOrEmpty(a)){
				
				var resolveUrl = nlapiResolveURL('SUITELET','customscript_erp_create_to_import_sl','customdeploy_erp_create_to_import_sl', true);  
				
				var slCall = new Array();		
				slCall['User-Agent-x'] = 'SuiteScript-Call';
			    
			    var params = [];
				params['transactionRecord'] = a; 
				
				var reqUrl = nlapiRequestURL(resolveUrl, params, slCall);
				nlapiLogExecution('AUDIT', 'reqUrl', reqUrl);
			
			}
		}
		catch(ex){
			//SSS_REQUEST_TIME_EXCEEDED
			
			nlapiSetFieldValue('custrecord_transorder_created_stat', 2);
			nlapiSetFieldValue('custrecord_transord_memo', ex.toString());
		}
		
	}
	
}


function getItemPrices(idSubsidiary, idLocation, arrUniqueItems){

		var idFromLocation = idLocation; 
		var uniqueItems = arrUniqueItems; 

		var arSavedSearchResultsItm = null;
		var arSaveSearchFiltersItm = new Array();
		var arSavedSearchColumnsItm = new Array();

		var resultsArray = new Array(); // important array
		var idArray = new Array(); // important array
		var itemObjects = [];

		var strSavedSearchIDItm = null;

		arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null,'anyof', uniqueItems));
		var mainLocation = getMainLocation(idSubsidiary); 

		nlapiLogExecution('DEBUG', 'getItemPrices [' + idSubsidiary + ']', idLocation + " " + mainLocation );
		
		if(!isNullOrEmpty(mainLocation)){
			arSaveSearchFiltersItm.push(new nlobjSearchFilter('inventorylocation',null, 'anyof', [idLocation, mainLocation]));
		}else{
			arSaveSearchFiltersItm.push(new nlobjSearchFilter('inventorylocation',null, 'is', idLocation));
		}


		arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('inventorylocation'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn(	'locationaveragecost'));

		arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);

		
		
		if (!isNullOrEmpty(arSavedSearchResultsItm)) {
		
			for (var i = 0; i < arSavedSearchResultsItm.length; i++) {

				var obj = new Object();
				obj.id = arSavedSearchResultsItm[i].getValue('internalid');			
				obj.location = arSavedSearchResultsItm[i].getValue('inventorylocation');
				obj.locavecost = arSavedSearchResultsItm[i].getValue('locationaveragecost');
//				obj.locationname = arSavedSearchResultsItm[i].getText('inventorylocation');
//				obj.name = arSavedSearchResultsItm[i].getValue('name');	

				itemObjects.push(obj);

			}
		}

		return itemObjects;

}

function setItemPrices(arrPriceList, objTransferOrder, arrLocations){

	//nlapiLogExecution('DEBUG', 'setItemPrices [' + arrLocations + ']', JSON.stringify(arrLocations));

	var intItemCount = objTransferOrder.getLineItemCount('item');
	
	nlapiLogExecution('DEBUG', 'to item count', intItemCount);
	var statPendingApproval = false; 
	
	var idSubsidiary = objTransferOrder.getFieldValue('subsidiary'); 
	var idToLocation = objTransferOrder.getFieldValue('location'); 
	var idMainLocation = getMainLocation(idSubsidiary)
	
	
	nlapiLogExecution('DEBUG', 'setItemPrices [' + idSubsidiary + ']', idToLocation + " " + idMainLocation );
	
		
	for (var j = 1; j <= intItemCount; j++){
		

		nlapiSetLineItemValue('item', 'rate', j, 0);
		var lineItem = objTransferOrder.getLineItemValue('item', 'item', j);

		var itemObject = _(arrPriceList).filter(function(x) {
		  return (x.id == lineItem && x.location == idToLocation); //TODO change to variable
		});
		
		
		if(!isNullOrEmpty(itemObject)){
			
			if(itemObject[0].locavecost <= 0){
				itemObject = _(arrPriceList).filter(function(x) {
					  return (x.id == lineItem && x.location == idMainLocation); //TODO change to variable
				});
			}
			
			nlapiLogExecution('DEBUG', 'perLine [' + j + '] [' + lineItem + ']', JSON.stringify(itemObject) );

		
			if(!isNullOrEmpty(itemObject)){
				
				if(itemObject[0].locavecost > 0){
					objTransferOrder.setLineItemValue('item', 'rate', j, itemObject[0].locavecost);
					objTransferOrder.setLineItemValue('item', 'custcol_erp_to_memo', j, itemObject[0].location);
				}
				else{
					objTransferOrder.setLineItemValue('item', 'rate', j, 0);
					objTransferOrder.setLineItemValue('item', 'custcol_erp_to_memo', j, 'not found');
					statPendingApproval = true;
				}
				
			}
		}else{
			objTransferOrder.setLineItemValue('item', 'rate', j, 0);
			objTransferOrder.setLineItemValue('item', 'custcol_erp_to_memo', j, 'not found');
			statPendingApproval = true; 
		}
		
	}
	
	if(statPendingApproval == true) objTransferOrder.setFieldValue('orderstatus', 'A'); //'A'); Means an item is missing a price

	return objTransferOrder; 

}





function getMainLocation(idSubsidiary){
			
			var mainLocationId = null;

			var searchFiltersMainLocation = [];
			searchFiltersMainLocation[0] = new nlobjSearchFilter('subsidiary', null, 'is', idSubsidiary);
			searchFiltersMainLocation[1] = new nlobjSearchFilter('custrecord_ka_main_warehouse', null, 'is', 'T');
			var searchColumnsMainLocation = [ new nlobjSearchColumn('internalid') ];
			var resultsMainLocation = nlapiSearchRecord('location', null, searchFiltersMainLocation, searchColumnsMainLocation);
			mainLocationId = resultsMainLocation[0].getValue('internalid');

			if(!isNullOrEmpty(resultsMainLocation)){
				 mainLocationId = resultsMainLocation[0].getValue('internalid');
			}

			return mainLocationId;
}


function getExistingRecordViaExternalID(strCustomRecordID, strExternalID)
{
	var custRecord = null;
	try
	{
		var processMssg = 'searching for existing record via external id.';

		var searchFilters = [ new nlobjSearchFilter('externalid', null, 'is', strExternalID) ];
		var searchColumns = [ new nlobjSearchColumn('internalid') ];

		var results = nlapiSearchRecord(strCustomRecordID, null, searchFilters, searchColumns);

		if (!isNullOrEmpty(results))
		{
			objResult = results[0];

			intInternalID = objResult.getId();// getValue('internalid');
			return intInternalID;
		}

	} catch (ex)
	{
		var errorStr = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() + '\n' : ex.toString();
		nlapiLogExecution('Error', 'processManualTask()', 'Error encountered while ' + processMssg + ' == ' + errorStr);
	}
	return custRecord;
	
	
}

function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}