/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope Public
 */


define(['N/record',
        'N/url',
        'N/http',
        'N/https',
        'N/search',
        'N/error',
        '../../ERP20/Closing Notes/momentlocalejs.js'],
		mainClosingNotesCS
    );

function mainClosingNotesCS(record, url, http, https, search, error, moment) {

    function alertPageLoaded(context) {

      var currentRecord = context.currentRecord;
      var customForm = currentRecord.getValue('customform');

      // Find web browser type
      var _navigator = {};

      for (var i in navigator) _navigator[i] = navigator[i];

      delete _navigator.plugins;
      delete _navigator.mimeTypes;

      jQuery('#navigator').text(JSON.stringify(_navigator));

       var navigt =   JSON.stringify(_navigator); console.log(navigt);
       currentRecord.setValue('custrecord_erp_navigator', navigt);

    	if(customForm != 124){

	    	var locId = currentRecord.getValue('custrecord_erp_dclose_shop_name');
	    	var dateCreated = currentRecord.getValue('custrecord_erp_date_created');
	    	console.log('alertPageLoaded: this date ' + dateCreated);

	    	//DATE && SALES
	    	var datesObject = setAllDates(currentRecord, url, https);

	    	setMiscInfo(currentRecord, url, https, error, locId ,datesObject.today, datesObject.today);
	    	setMiscInfo2(currentRecord, url, https, error, locId ,datesObject.weekstart, datesObject.weekend);
    	}
    }

 function fieldChanged(context) {

    	var currentRecord = context.currentRecord;
    	var customForm = currentRecord.getValue('customform');

    	if(customForm != 124){

		    	// console.log('fieldChanged: change: ' + context.fieldId);
          // console.log('fieldChanged: change: Line ' + context.line);


		        var name = context.fieldId; var periodType = '';

		        if(name == 'custrecord_erp_dclose_daily_goal' || name == 'custrecord_erp_dclose_daily_actual'){
		        	periodType =  'daily';

		      var  dailyActual = currentRecord.getValue('custrecord_erp_dclose_daily_actual');
	 				var  dailyActualHours = currentRecord.getValue('custrecord_erp_dclose_actual_hours');
	 				var salesLabHour = dailyActual / dailyActualHours;

	 				if(dailyActualHours ===  0 || dailyActualHours === '' ){
	 					currentRecord.setValue('custrecord_erp_dclose_saleslabhour', 0);
					}else{
						currentRecord.setValue('custrecord_erp_dclose_saleslabhour', salesLabHour);
					}
				}
		        else if(name == 'custrecord_erp_dclose_weekly_goal' || name == 'custrecord_erp_dclose_weekly_actual'){
		        	periodType =  'weekly';
				}
		        else if(name == 'custrecord_erp_dclose_period_goal' || name == 'custrecord_erp_dclose_period_actual'){
		        	periodType =  'period';
				}
		        else if(name == 'custrecord_erp_dclose_qtr_goal' || name == 'custrecord_erp_dclose_qtr_actual'){
		        	periodType =  'qtr';
				}
		        else if(name == 'custrecord_erp_date_created' || name == 'custrecord_erp_dclose_shop_name'){

		        	var locId = currentRecord.getValue('custrecord_erp_dclose_shop_name');
		        	var dateCreated = currentRecord.getValue('custrecord_erp_date_created');

		        	if(!isNullOrEmpty(locId) && !isNullOrEmpty(dateCreated)){
		        		var datesObject = setAllDates(currentRecord, url, https);
		        		setMiscInfo(currentRecord, url, https, error, locId, datesObject.today,  datesObject.today);
		        	}
		        }else if(name == 'custrecord_erp_dclose_actual_hours'){

	 				var  dailyActual = currentRecord.getValue('custrecord_erp_dclose_daily_actual');
	 				var  dailyActualHours = currentRecord.getValue('custrecord_erp_dclose_actual_hours');
	 				var salesLabHour = dailyActual / dailyActualHours;

	 				if(dailyActualHours ===  0 || dailyActualHours === '' ){
	 					currentRecord.setValue('custrecord_erp_dclose_saleslabhour', 0);
					}else{
						currentRecord.setValue('custrecord_erp_dclose_saleslabhour', salesLabHour);
					}
		        }
		        else if(name == 'custrecord_erp_dclose_returns' || name == 'custrecord_erp_dclose_ecomm_returns'){
		         var regular = currentRecord.getValue('custrecord_erp_dclose_ecomm_returns');
						 var total = currentRecord.getValue('custrecord_erp_dclose_returns');
						 var exEcomm = total - regular;

						 currentRecord.setValue('custrecord_erp_dclose_regreturn', exEcomm);
		        }

		    else if(name == 'custrecord_erp_dclose_netsuite_closing' || name == 'custrecord_erp_dclose_payment_terminal' ){

					var nsClosing = currentRecord.getValue('custrecord_erp_dclose_netsuite_closing');
		 			var terminal = currentRecord.getValue('custrecord_erp_dclose_payment_terminal');
		 			var varianceNSTerminal =  terminal - nsClosing;

		 			currentRecord.setValue('custrecord_erp_dclose_variance', varianceNSTerminal );
				}


		    else if(name == 'custrecord_erp_dclose_day_footfall' || name == 'custrecord_erp_dclose_day_transactioncnt' ){

					var ffall = currentRecord.getValue('custrecord_erp_dclose_day_footfall');
		 			var trancnt = currentRecord.getValue('custrecord_erp_dclose_day_transactioncnt');

		 			if(!ffall){ ffall = 1;
		 				currentRecord.setValue('custrecord_erp_dclose_day_conversion', '' );
		 			}else{
		 				var conversionday = trancnt / ffall;
		 				currentRecord.setValue('custrecord_erp_dclose_day_conversion', conversionday );
		 			}
				}

		        else if(name == 'custrecord_erp_dclose_wtd_footfall' || name == 'custrecord_erp_dclose_wtd_transactioncnt' ){

		      var ffall = currentRecord.getValue('custrecord_erp_dclose_wtd_footfall');
		 			var trancnt = currentRecord.getValue('custrecord_erp_dclose_wtd_transactioncnt');

		 			if(!ffall){ ffall = 1;
		 				currentRecord.setValue('custrecord_erp_dclose_wtd_conversion', '' );
		 			}else{
		 				var conversionday = trancnt / ffall;
		 				currentRecord.setValue('custrecord_erp_dclose_wtd_conversion', conversionday );
		 			}


				}
		        if(!isNullOrEmpty(periodType)){
		        	calculateToGoals(currentRecord, periodType);
		        	if(periodType == 'daily'){
		        	}
    	   }

         if(context.fieldId =='custrecord_cnterm1_moneris' && context.value !== 0){
              currentRecord.selectLine({sublistId: 'recmachcustrecord_cnterm1_link',line:context.line});

              var recAmount =  currentRecord.getCurrentSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_amount',});
              var recMoneris =  currentRecord.getCurrentSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_moneris',});

              var recVariance =  recMoneris - recAmount;

             currentRecord.setCurrentSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_variance',value: recVariance});
            }
    }
}



    function setUPTs(currentRecord,locId,date1,date2){

                var mySearch = search.load({
                    id: 'customsearch1188',
                });

    			mySearch.filterExpression = [['location', 'anyof' , locId],
                 							 'and',
                 							 ['datecreated', 'within', [date1, date2]]
                 							];

    			var searchResult = mySearch.run().getRange({
                  start: 0,
                  end: 100
                });

            	  console.log('setUPTs: searching:' + date1 + ' : ' +date2 + " result:" + searchResult.length);

                  if(!isNullOrEmpty(searchResult)){

      	                var columns = searchResult[0].columns;
      	                var objResult = searchResult[0];

      	    			var objUPTSPT = {};

      	    			objUPTSPT.location = objResult.getValue(columns[0]);
      	    			objUPTSPT.upt = objResult.getValue(columns[1]);
      	    			objUPTSPT.spt = objResult.getValue(columns[2]);
      	    			objUPTSPT.aur = objResult.getValue(columns[3]);

      	    			var upt = objResult.getValue(columns[1]);
	      	  			     upt = Number(upt).toFixed(1);
	      	  			var spt = objResult.getValue(columns[2]);
	      	  			     spt = Number(spt).toFixed(0);

	      	  			var aur = objResult.getValue(columns[3]);
	      	  			     aur = currentRecord.getValue('custrecord_erp_dclose_daily_actual')/aur;
	      	  			     aur = Number(aur).toFixed(0);

		      	  		currentRecord.setValue('custrecord_erp_dclose_upt_units_pertrans', upt);
			      	  	currentRecord.setValue('custrecord_erp_dclose_sptdpt', spt);
			      	  	currentRecord.setValue('custrecorderp_dclose_aur', aur);

      	    			console.log('setUPTs: upts are here: ' + JSON.stringify(objUPTSPT));

                  }
    }

  function setShopClosing(currentRecord,locId,date1,date2){


        var searchShopClosing = search.load({
            id: 'customsearch_erp_script_cn_closing',
        });

        searchShopClosing.filterExpression = [['location', 'anyof' , [locId]],
         							 'and',
         							 ['trandate', 'within', [date1, date2]]
         							];

		var searchResult = searchShopClosing.run().getRange({
          start: 0,
          end: 100
        });

    	  console.log('setShopClosing: searching:' + date1 + ' : ' +date2 + " result:" + searchResult.length);

          if(!isNullOrEmpty(searchResult)){

	                var columns = searchResult[0].columns;
	                var objResult = searchResult[0];

	    			var objClosingSales = new Object();

	    			objClosingSales.location = objResult.getValue(columns[0]);
	    			objClosingSales.closingsales = objResult.getValue(columns[1]);
	    			objClosingSales.col2 = objResult.getValue(columns[2]);
	    			objClosingSales.col3 = objResult.getValue(columns[3]);
	    			objClosingSales.col4 = objResult.getValue(columns[4]);
//	    		objClosingSales.col5 = objResult.getValue(columns[5]);

	    			currentRecord.setValue('custrecord_erp_dclose_netsuite_closing',objClosingSales.closingsales);
	    			console.log('setShopClosing: upts are here: ' + JSON.stringify(objClosingSales));

          }

}

    function setMiscInfo(currentRecord, url, https, error, locationId, tranDate1, tranDate2){

    	//console.log('LOG2: get here if you can');
    	try{
    		var locationId = currentRecord.getValue('custrecord_erp_dclose_shop_name');

//        	console.log('this date: ' + todayDate);

        	var disposalLink = url.resolveScript({
                scriptId            : 'customscript_erp_sdc_sl_misc', //'customscript_erp_shopclosing_sltest', //
                deploymentId        : 'customdeploy_erp_sdc_sl_misc', //'customdeploy_erp_shopclosing_sl', //
                returnExternalUrl   : false,
                params              : null
            });

        	disposalLink = disposalLink +
        					'&location=' + locationId +
        					'&trandate1=' + tranDate1 +
        					'&trandate2=' + tranDate2;

        	console.log('LOG2: disposalLink' + disposalLink);

    		var response = https.get.promise({url:disposalLink}).then(

	    			function(result){

	    				console.log("this result via the new url resolution: " + result.body);
	    	    		var miscObj  = JSON.parse(result.body);
                console.log(JSON.stringify(miscObj));

              if(!isNullOrEmpty(miscObj)){

							currentRecord.setValue('custrecord_erp_dclose_netsuite_closing', miscObj.closing.closingsales);
							currentRecord.setValue('custrecord_erp_dclose_upt_units_pertrans', miscObj.upt.upt);
							currentRecord.setValue('custrecord_erp_dclose_sptdpt',  miscObj.upt.spt);
							var actual = currentRecord.getValue('custrecord_erp_dclose_daily_actual');

              createTerminalSalesObj(currentRecord,miscObj.posSales);

							if(actual && miscObj.upt.aur > 0){

								//console.log('aur this' + miscObj.upt.aur);
								currentRecord.setValue('custrecorderp_dclose_aur',  actual/miscObj.upt.aur);

							}
              //If values are null set to 0
							currentRecord.setValue('custrecord_erp_dclose_day_transactioncnt',  miscObj.upt.trans);
							currentRecord.setValue('custrecord_erp_dclose_peak_times', miscObj.peaktime);
							currentRecord.setValue('custrecord_erp_dclose_fd',(Math.abs(miscObj.posdiscount.fd)||0));
							currentRecord.setValue('custrecord_erp_dclose_employee_discounts',(Math.abs(miscObj.posdiscount.ed)||0));
              //Set email capture rate

              var emailCount = ((parseInt(miscObj.emailCaptures[1]) || 0) + (parseInt(miscObj.emailCaptures[3]) || 0));

              currentRecord.setValue('custrecord_erp_dclose_email_count',emailCount);
              // ensure that you cannot divide by 0  , and there is always a value
               var pctEmail = (parseInt(miscObj.emailCaptures[3]) || 1) /
              (
                (parseInt(miscObj.emailCaptures[1]) ||0) +
                (parseInt(miscObj.emailCaptures[3]) ||0)
               );
              currentRecord.setValue('custrecord_erp_dclose_email_convers',Number(pctEmail).toFixed(2)*100 );
                }

	    	    	}
	    	).catch(
	    		function onRejected(reason) {
	    			console.log('misc error:' + reason);
	    		}
	    	);

    		console.log('LOG2: ' + response.body);

    		return response;
        	}catch(ex){ return null;}
          window.scrollTo(500, 0);
    }



    function setMiscInfo2(currentRecord, url, https, error, locationId, tranDate1, tranDate2){

    	try{

    		var locationId = currentRecord.getValue('custrecord_erp_dclose_shop_name');



        	var disposalLink = url.resolveScript({
                scriptId            : 'customscript_erp_sdc_sl_misc', //'customscript_erp_shopclosing_sltest', //
                deploymentId        : 'customdeploy_erp_sdc_sl_misc', //'customdeploy_erp_shopclosing_sl', //
                returnExternalUrl   : false,
                params              : null
            });

        	disposalLink = disposalLink +
        					'&location=' + locationId +
        					'&trandate1=' + tranDate1 +
        					'&trandate2=' + tranDate2;

        	console.log('LOG2: disposalLink' + disposalLink);

    		var response = https.get.promise({url:disposalLink}).then(

	    			function(result){

	    	    		var miscObj  = JSON.parse(result.body);

	    	    		if(!isNullOrEmpty(miscObj)){

	    	    			currentRecord.setValue('custrecord_erp_dclose_wtd_transactioncnt',  miscObj.upt.trans);

	    	    		}

	    	    	}

	    	).catch(
	    		function onRejected(reason) {
	    			console.log('misc error:' + reason);
	    		}
	    	);

    		return response;

        }catch(ex){ return null;}

    }


    function setAllDates(currentRecord, url, https){

    	var datesObject = {};


    	var inputDate = currentRecord.getValue('custrecord_erp_date_created');
    	//console.log('this input date: ' + inputDate);

    	var formatDate = moment(inputDate); //,'MM/DD/YYYY');
    	var todayDate = formatDate.format('L');

    	var accountPeriodObject = getAccountingPeriod(url,https, todayDate);

    	if(!isNullOrEmpty(accountPeriodObject)){

	    	var accountPeriodObject = JSON.parse(accountPeriodObject.body);
	    	var dateRange = accountPeriodObject;

        var lastWeek = moment(inputDate).subtract(1,'week').format('L');

	    	var weekStart = moment(inputDate).startOf('isoweek').format('L');
	    	var weekEnd = moment(inputDate).endOf('isoweek').format('L');
        //Last year This week
        var lyWeekStart = moment(inputDate).subtract(1, 'years').startOf('isoweek').format('L');
        var lyWeekWTD = moment(inputDate).subtract(1, 'years').format('L');

	    	var locId = currentRecord.getValue('custrecord_erp_dclose_shop_name');

        //Today Date range
    		currentRecord.setValue('custrecord_day_daterange', todayDate);
    		getSalesRangeSL(locId, "daily", todayDate, todayDate, currentRecord, url, https);
    		getSalesRangeSL(locId, 'dailycredit', todayDate, todayDate, currentRecord, url, https);
        //Last Week Date range
        currentRecord.setValue('custrecord_last_week_daterange',lastWeek);
        getSalesRangeSL(locId, "last_week", lastWeek, lastWeek, currentRecord, url, https);
        //This Week
    		currentRecord.setValue('custrecord_week_daterange', weekStart + ' - ' + weekEnd);
    		getSalesRangeSL(locId, "weekly", weekStart, weekEnd, currentRecord, url, https);
        //Last year this week
        currentRecord.setValue('custrecord_ly_week_daterange', lyWeekStart +' - ' + lyWeekWTD);
        getSalesRangeSL(locId, "ly_week", lyWeekStart, lyWeekWTD, currentRecord, url, https);
        //This period
    		currentRecord.setValue('custrecord_period_daterange', accountPeriodObject.period.start + "  - " + accountPeriodObject.period.end);
    		getSalesRangeSL(locId, "period", accountPeriodObject.period.start,  accountPeriodObject.period.end, currentRecord, url, https);

    		currentRecord.setValue('custrecord_quarter_daterange', accountPeriodObject.quarter.start + "  - " + accountPeriodObject.quarter.end);
    		getSalesRangeSL(locId, "qtr", accountPeriodObject.quarter.start, accountPeriodObject.quarter.end, currentRecord, url, https);



    	}

    	datesObject.today = todayDate;
    	datesObject.weekstart = weekStart;
    	datesObject.weekend = weekEnd;

    	return datesObject;
    }


    function getSalesRangeSL(locId, mode, date1, date2, currentRecord, url, https) {

    	try{
	    	var disposalLink = url.resolveScript({
	            scriptId            : 'customscript_erp_shop_daily_closing_sl', //'customscript_erp_shopclosing_sltest', //
	            deploymentId        : 'customdeploy_erp_shop_daily_closing_sl', //'customdeploy_erp_shopclosing_sl', //
	            returnExternalUrl   : false,
	            params              : null
	        });


          //What does mode do????

	    	var modeTxt =mode;

	    	if(mode == 'daily' ||
           mode == 'last_week'||
	    	   mode == 'weekly'||
           mode == 'ly_week'||
	    	   mode == 'period'||
	    	   mode == 'qtr' ){

	    	   mode = 'salesinfo';

	    	}else if('dailycredit'){

	    	   mode = 'creditinfo';

	    	}else{ mode = ''; }


	    	var params = [];
	    	params['locationId'] = locId;
	    	params['transactionDate1'] = date1;
	    	params['transactionDate2'] = date2;
	    	params['mode'] = mode; //'salesinfo';


	    	disposalLink = disposalLink +
	    								'&locationId=' + locId +
								    	'&transactionDate1=' + date1 +
								    	'&transactionDate2=' + date2 +
								    	'&mode=' + mode;

	    	//console.log('SL' + disposalLink);


	    	https.get.promise({url:disposalLink}).then(

	    			function(result){

	    				//console.log("this result via the new url resolution: " + result.body);
	    	    		var salesObj = JSON.parse(result.body);
            console.log("this result via the new url resolution: "+ modeTxt + result.body);


	    	    	var salesArrIds = [];
	    				var salesArrObj = [];

	    				salesArrIds = salesObj.ids;
	    				salesArrObj = salesObj.obj;

	    	    var salesIndex = salesArrIds.indexOf("213213");

            if (salesIndex > -1) {
							var obj = salesArrObj[salesIndex];
							var netSales = obj.netsales_fx;
							var netSalesIncTax = obj.netsales_inctax_fx;

							var currency = salesArrObj[salesIndex].currency;
							//nlapiSetFieldValue('custrecord_erp_dclose_currency', currency);

							if(modeTxt != 'dailycredit'){
								currentRecord.setValue('custrecord_erp_dclose_' + modeTxt + '_actual', netSales);



								if(modeTxt == 'daily'){

									currentRecord.setValue('custrecord_erp_dclose_daily_inctax', netSalesIncTax);
								}

							}else{
								currentRecord.setValue('custrecord_erp_dclose_returns', netSales);
							}
						}else{
							currentRecord.setValue('custrecord_erp_dclose_' + modeTxt + '_actual', 0);
						}
	    	    	}
	    	).catch(function onRejected(reason) {
	    		currentRecord.setValue('custrecord_erp_dclose_' + modeTxt + '_actual', 0);
	          });

    	}catch(ex){}
	    	//return slResponse;
    }

    function getAccountingPeriod(url,https, todayDate){

    	try{
    	var paramsarr = new Array();
    	paramsarr['transactionDate'] = todayDate;

    	//console.log('this date: ' + todayDate);

    	var disposalLink = url.resolveScript({
            scriptId            : 'customscript_erp_shop_daily_closing_sl', //'customscript_erp_shopclosing_sltest', //
            deploymentId        : 'customdeploy_erp_shop_daily_closing_sl', //'customdeploy_erp_shopclosing_sl', //
            returnExternalUrl   : false,
            params              : null
        });

    	disposalLink = disposalLink + '&transactionDate=' + todayDate;

		var response = https.get({url:disposalLink});
		console.log('LOG1: ' + response.body);

		return response;

    	}catch(ex){ return null;}

    }


    function getAjax(){

		var promise = new Promise(function(resolve, reject){

			var url = 'https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=550&deploy=1',
			    isAsync = true,
			    xhr = new XMLHttpRequest();

			xhr.addEventListener('load', function (event) {
			    if (xhr.readyState === 4) {
			        if (xhr.status === 200) {
			            resolve(xhr.responseText);

			        	console.log(  new Date());
			        }
			        else {
			            reject( xhr.statusText );

			        	console.log( xhr.statusText );
			        }
			     }
			});
			xhr.addEventListener('error', function (event) {
			    reject( xhr.statusText );
			});
			xhr.open('GET', url, isAsync);
			    xhr.send();

		});

        	return promise;

    }

    function calculateToGoals(currentRecord, periodType){

    var sales 		= 	currentRecord.getValue('custrecord_erp_dclose_'+ periodType +'_actual');
		 var goal		= 	currentRecord.getValue('custrecord_erp_dclose_'+ periodType +'_goal');
		 var fSales 	= 	0; fSales = Math.round(sales);
		 var fGoal		= 	0; fGoal = Math.round(goal);

		 var diff = fSales - fGoal;
	     var perc = 0;

	    // console.log('fsale and fgoal' + fSales + ' ' + fGoal);

	     if (!isNullOrEmpty(fGoal) && (fGoal > 0)) {

	    	 if(!isNaN(fGoal)){
	    		 	perc = (fSales / fGoal) * 100;
		    		perc = Math.round(perc);
	    	 }
	    }

		 currentRecord.setValue('custrecord_erp_dclose_'+ periodType +'_togoal',diff);
		 currentRecord.setValue('custrecord_erp_dclose_'+ periodType +'_ptogoal',perc);

    }


function createTerminalSalesObj(currentRecord,posSales){

  console.log('Terminals: ' + JSON.stringify(posSales));
    var terminals = Object.entries(posSales);
    var lineItems = [];

  for(var j=0;j < terminals.length;j++){
      terminals[j][1].payments.forEach(function(paymentTypes){
      var lines = {};
      lines.terminal=terminals[j][0];
      lines.paymentMethod = paymentTypes.paymentMethod;
      lines.amount =paymentTypes.amount;
      lines.key = (lines.terminal+'-'+lines.paymentMethod);
      lineItems.push(lines);
      });
  }

//check for existing terminal entries and compare objects
var sublistLen = currentRecord.getLineCount({sublistId: 'recmachcustrecord_cnterm1_link'});


    if(sublistLen > 0 ){
          console.log('compareAddTerminalSales');
       compareAddTerminalSales(currentRecord,lineItems,sublistLen);

    }
   else{
      console.log('addTerminalSales ');
      addTerminalSales(currentRecord,lineItems);
      }
}

function addTerminalSales (currentRecord,lineItems){

  for(var i=0;i < lineItems.length;i++){
    currentRecord.selectNewLine({sublistId: 'recmachcustrecord_cnterm1_link',});
    currentRecord.setCurrentSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_term_id',value: lineItems[i].terminal});
    currentRecord.setCurrentSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_ns',value: lineItems[i].paymentMethod});
    currentRecord.setCurrentSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_amount',value: Math.round(lineItems[i].amount*100)/100});
    if(lineItems[i].moneris) {
      currentRecord.setCurrentSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_moneris',value: lineItems[i].moneris});
      currentRecord.setCurrentSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_variance',value:lineItems[i].moneris - Math.round(lineItems[i].amount*100)/100});

    }
    currentRecord.setCurrentSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_variance',value: - Math.round(lineItems[i].amount*100)/100});
    currentRecord.setCurrentSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_moneris',value: 0});
    currentRecord.commitLine({sublistId: 'recmachcustrecord_cnterm1_link'});
  }
}

function compareAddTerminalSales(currentRecord,lineItems,sublistLen){

//Grab existing sublist
  var lineItemsPrev = [];
  var newLines = [];
  var newTermKeys = [];

    for(var f=0;f < sublistLen;f++){
        var linesPrev = {};
        linesPrev.terminal = currentRecord.getSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_term_id',line:f});
        linesPrev.paymentMethod = currentRecord.getSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_ns',line:f});
        linesPrev.moneris =  currentRecord.getSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_moneris',line:f});
        linesPrev.variance = currentRecord.getSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_variance',line:f});
        linesPrev.amount = currentRecord.getSublistValue({sublistId: 'recmachcustrecord_cnterm1_link',fieldId: 'custrecord_cnterm1_amount',line:f});
        linesPrev.line = f;
        linesPrev.key=(linesPrev.terminal+'-'+linesPrev.paymentMethod);
        lineItemsPrev.push(linesPrev);
    }
            lineItemsPrev.forEach(function(itemsA){
             lineItems.forEach(function(itemsB){
                  if(itemsA.key === itemsB.key){
                    var newPayment= {};
                    newPayment.line = itemsA.line;
                    newPayment.paymentMethod = itemsA.paymentMethod;
                    newPayment.amount =itemsB.amount;
                    newPayment.moneris =itemsA.moneris;
                    newPayment.variance =itemsB.amount -itemsA.moneris;
                    newPayment.key = itemsB.key;
                    newPayment.terminal =itemsB.terminal;
                    newLines.push(newPayment);
                    newTermKeys.push(itemsB.key);
                  }
                });
             });
            var lineStart = newLines.length;
            var newTerminals = lineItems.filter(function(item){
              return(newTermKeys.indexOf(item.key) === -1);
            });

            newTerminals.forEach(function(term,index){
                 term.line = lineStart+index;
                 term.moneris =0;
                newLines.push(term);
             });
console.log('compareAddTerminalSales'+JSON.stringify(newLines));
removeTerminalSales(currentRecord,sublistLen);
addTerminalSales(currentRecord,newLines);

}
function removeTerminalSales(currentRecord,sublistLen){

console.log('Sub length '+sublistLen);
//sublistLen=2;
for(var i =1;  i <= sublistLen;i++){
  currentRecord.selectLine({sublistId: 'recmachcustrecord_cnterm1_link',line: 0});
  currentRecord.removeLine({sublistId: 'recmachcustrecord_cnterm1_link',line: 0,ignoreRecalc: true});
  //currentRecord.commitLine({sublistId: 'recmachcustrecord_cnterm1_link'});
  console.log('line: '+ i);
  }

}


function updateMemoFieldWhenItemFieldIsChanged(context) {
        if(context.fieldId === 'item') {
          //context.currentRecord.setValue('memo', 'item field is changed');
        	console.log('track this please');
        }
    }
function isNullOrEmpty(valueStr){
        return(valueStr === null || valueStr === "" || valueStr === undefined );
    }

return({
      pageInit: alertPageLoaded,
      fieldChanged: fieldChanged
    });
}
