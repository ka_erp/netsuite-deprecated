/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Dec 2016     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function createTransferOrder_UE_BS(type){
 
	nlapiLogExecution('DEBUG', 'type', type); 
	if( type == 'edit'){
		
		nlapiLogExecution('DEBUG', 'got here', 'why are you here'); 
		
	
//	var sc = getExistingRecordViaExternalID('transferorder', 'mismo1'); 
//	
//	nlapiLogExecution('DEBUG', 'this to with externalid', JSON.stringify(sc)); 
//	
//	if(sc == null){
		
		var idSubsidiary = nlapiGetFieldValue('custrecord_erp_toi_subsidiary');
		var idFromLocation = nlapiGetFieldValue('custrecord_erp_toi_fromloc');
		var idToLocation = nlapiGetFieldValue('custrecord_erp_toi_tolocation');
		var idTransferOrderType = nlapiGetFieldValue('custrecord_toi_transfertype');
		//var idTransferType = nlapiGetFieldValue('');		
		
		var idItem = nlapiGetFieldValue('custrecord_erp_toi_item');		
			
		var transferOrder = nlapiCreateRecord('transferorder');
		
		
		var intItemCount = nlapiGetLineItemCount('recmachcustrecord_transorderline_link');
		nlapiLogExecution('AUDIT', 'Count', intItemCount);

		transferOrder.setFieldValue('customform', 147); // TODO: Put this in a parameter
		transferOrder.setFieldValue('subsidiary', idSubsidiary); 
		transferOrder.setFieldValue('location', idFromLocation); 
		transferOrder.setFieldValue('transferlocation', idToLocation); 
		transferOrder.setFieldValue('employee',298465); // nlapiGetUser());
		transferOrder.setFieldValue('custbody_ka_order_type', idTransferOrderType);	
		transferOrder.setFieldValue('orderstatus', 'B'); //'A');
		
		
		transferOrder.setFieldValue('custbody_ka_to_expected_ship_date', nlapiGetFieldValue('custrecord_toi_expectedshipdt')); //'A');
		transferOrder.setFieldValue('custbody_ka_to_expected_recv_date', nlapiGetFieldValue('custrecord_toi_expectedrecvdt')); //'A');
		
		
		//transferOrder.setFieldValue('externalid','mismo1');	

		//var intItemCount = nlapiGetLineItemCount('recmachcustrecord_transorderline_link');
		
		var itemsArray = []; 
		
		for (var j = 1; j <= intItemCount; j++){
			
			var a =  nlapiGetLineItemValue('recmachcustrecord_transorderline_link', 'custrecord_transorderline_item', j);
			var qty =  nlapiGetLineItemValue('recmachcustrecord_transorderline_link', 'custrecord_toil_quantity', j);
			var amount =  nlapiGetLineItemValue('recmachcustrecord_transorderline_link', 'custrecord_transordline_amount', j);
			//nlapiLogExecution('AUDIT', 'Item', a);
			
			transferOrder.selectNewLineItem('item');
			transferOrder.setCurrentLineItemValue('item', 'item', a);
			transferOrder.setCurrentLineItemValue('item', 'quantity', qty);
			transferOrder.setCurrentLineItemValue('item', 'rate', 0);	
			transferOrder.commitLineItem('item', true);
			
			itemsArray.push( {"id":a}); 
			
		}
		
		var uniqueItems = _.pluck(itemsArray, 'id'); 

		var itemPrices = getItemPrices(idSubsidiary, idToLocation, uniqueItems); 
		transferOrder = setItemPrices(itemPrices, transferOrder, [idToLocation]); 
		
		
		
		
		var a = nlapiSubmitRecord(transferOrder);
		nlapiLogExecution('DEBUG', 'this new to', a);
	
		
		nlapiSetFieldValue('custrecord_erp_toi_transferref', a);
	

//	}else{
		
//		nlapiLogExecution('DEBUG', 'loading this new order',sc);
//		var transferOrder = nlapiLoadRecord('transferorder', sc);
//		
//		var idItem = nlapiGetFieldValue('custrecord_erp_toi_item');	
//		
//		transferOrder.selectNewLineItem('item');
//		transferOrder.setCurrentLineItemValue('item', 'item', idItem);
//		transferOrder.setCurrentLineItemValue('item', 'quantity', 1);
//		transferOrder.setCurrentLineItemValue('item', 'rate', 100);	
//		transferOrder.commitLineItem('item', true);
//		
//		var a = nlapiSubmitRecord(transferOrder);
//		nlapiLogExecution('DEBUG', 'this new to', a);
		
//	}
	}
	
}


function getItemPrices(idSubsidiary, idLocation, arrUniqueItems){

		var idFromLocation = idLocation; 
		var uniqueItems = arrUniqueItems; 

		var arSavedSearchResultsItm = null;
		var arSaveSearchFiltersItm = new Array();
		var arSavedSearchColumnsItm = new Array();

		var resultsArray = new Array(); // important array
		var idArray = new Array(); // important array
		var itemObjects = [];

		var strSavedSearchIDItm = null;

		arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null,'anyof', uniqueItems));
		var mainLocation = getMainLocation(idSubsidiary); 

		if(!isNullOrEmpty(mainLocation)){
			arSaveSearchFiltersItm.push(new nlobjSearchFilter('inventorylocation',null, 'anyof', [idFromLocation,mainLocation]));
		}else{
			arSaveSearchFiltersItm.push(new nlobjSearchFilter('inventorylocation',null, 'is', idFromLocation));
		}


		arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('inventorylocation'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn(	'locationaveragecost'));

		arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);

		
		
		if (!isNullOrEmpty(arSavedSearchResultsItm)) {
		
			for (var i = 0; i < arSavedSearchResultsItm.length; i++) {
				nlapiLogExecution('DEBUG','to lines',arSavedSearchResultsItm[i].getValue('internalid')); 

				var obj = new Object();
				obj.id = arSavedSearchResultsItm[i].getValue('internalid');
				obj.name = arSavedSearchResultsItm[i].getValue('name');				
				obj.location = arSavedSearchResultsItm[i].getValue('inventorylocation');
				obj.locationname = arSavedSearchResultsItm[i].getText('inventorylocation');
				obj.locavecost = arSavedSearchResultsItm[i].getValue('locationaveragecost');

				itemObjects.push(obj);

			}
		}

		return itemObjects;

}

function setItemPrices(arrPriceList, objTransferOrder, arrLocations){

	nlapiLogExecution('DEBUG', 'setItemPrices [' + arrLocations + ']', JSON.stringify(arrPriceList));

	var intItemCount = objTransferOrder.getLineItemCount('item');
		
	//var itemsArray = []; 
		
	for (var j = 1; j <= intItemCount; j++){
		

		nlapiSetLineItemValue('item', 'rate', j, 0);
		var lineItem = objTransferOrder.getLineItemValue('item', 'item', j);

		nlapiLogExecution('AUDIT', 'item:setItemPrices:item [' + lineItem + ']', lineItem);

		var itemObject = _(arrPriceList).filter(function(x) {
		  return (x.id == lineItem && x.location == 7);//arrLocations[0]); //
		});

		if(!isNullOrEmpty(itemObject)){

			
			nlapiSetLineItemValue('item', 'rate', j, itemObject[0].locavecost);
			nlapiLogExecution('AUDIT', 'item:setItemPrices:locave for [' + lineItem + ']', itemObject[0].locavecost);
		}
		
	}

	return objTransferOrder; 

}



function getMainLocation(idSubsidiary){
			//var idSubsidiary = transferOrder.subsidiary;
			var mainLocationId = null;

			var searchFiltersMainLocation = [];
			searchFiltersMainLocation[0] = new nlobjSearchFilter('subsidiary', null, 'is', idSubsidiary);
			searchFiltersMainLocation[1] = new nlobjSearchFilter('custrecord_ka_main_warehouse', null, 'is', 'T');
			var searchColumnsMainLocation = [ new nlobjSearchColumn('internalid') ];
			var resultsMainLocation = nlapiSearchRecord('location', null, searchFiltersMainLocation, searchColumnsMainLocation);
			mainLocationId = resultsMainLocation[0].getValue('internalid');

			if(!isNullOrEmpty(resultsMainLocation)){
				 mainLocationId = resultsMainLocation[0].getValue('internalid');
			}

			return mainLocationId;
}


function getExistingRecordViaExternalID(strCustomRecordID, strExternalID)
{
	var custRecord = null;
	try
	{
		var processMssg = 'searching for existing record via external id.';

		var searchFilters = [ new nlobjSearchFilter('externalid', null, 'is', strExternalID) ];
		var searchColumns = [ new nlobjSearchColumn('internalid') ];

		var results = nlapiSearchRecord(strCustomRecordID, null, searchFilters, searchColumns);

		if (!isNullOrEmpty(results))
		{
			objResult = results[0];
//			intInternalID = objResult.getId();// getValue('internalid');
//			if (!isNullOrEmpty(intInternalID))
//			{
//				custRecord = nlapiLoadRecord(strCustomRecordID, intInternalID);
//			}
			
			intInternalID = objResult.getId();// getValue('internalid');
			return intInternalID;
		}

	} catch (ex)
	{
		var errorStr = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() + '\n' : ex.toString();
		nlapiLogExecution('Error', 'processManualTask()', 'Error encountered while ' + processMssg + ' == ' + errorStr);
	}
	return custRecord;
	
	
}

function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}